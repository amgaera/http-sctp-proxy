/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * Methods used to encode and decode HTTP-SCTP messages.
 */

#include <lzma.h>
#include <errno.h>
#include <libsoup/soup.h>

#include "encapsulation.h"


#define COMPRESSION_LEVEL 6
#define INTEGRITY_CHECK LZMA_CHECK_CRC32
#define DECODE_MEMORY_LIMIT UINT64_MAX
#define DECODE_FLAGS LZMA_TELL_NO_CHECK | LZMA_TELL_UNSUPPORTED_CHECK


GQuark
http_sctp_message_error_quark(void)
{
	static GQuark quark = 0;

	if (quark == 0) {
		quark = g_quark_from_static_string("http-sctp-message-error-quark");
	}

	return quark;
}


/*
 * Encode the HTTP headers using the LZMA algorithm.
 */
static guint8*
encode_headers(guint8 *headers, gsize headers_length,
               gsize *compressed_hdrs_len, GError **error)
{
	gsize buffer_size;
	guint8 *compressed_hdrs;
	lzma_ret return_code;
	lzma_stream stream = LZMA_STREAM_INIT;

	buffer_size = lzma_stream_buffer_bound(headers_length);
	compressed_hdrs = g_new(guint8, buffer_size);

	return_code = lzma_easy_buffer_encode(COMPRESSION_LEVEL, INTEGRITY_CHECK,
	                                      NULL, headers, headers_length,
	                                      compressed_hdrs, compressed_hdrs_len,
	                                      buffer_size);

	if (return_code != LZMA_OK) {
		g_set_error(error, HTTP_SCTP_MESSAGE_ERROR,
		            HTTP_SCTP_MESSAGE_ERROR_LZMA,
		            "lzma_easy_buffer_encode error %d", return_code);

		return NULL;
	}

	return compressed_hdrs;
}


/*
 * Extract the value of the Content-Length header from the HTTP headers.
 */
static gssize
get_content_length(gchar *headers, gsize headers_length, GError **error)
{
	guint result;
	SoupMessageHeaders *parsed_headers;

	parsed_headers = soup_message_headers_new(SOUP_MESSAGE_HEADERS_REQUEST);

	if (!soup_headers_parse(headers, headers_length, parsed_headers)) {
		g_set_error(error, HTTP_SCTP_MESSAGE_ERROR,
		            HTTP_SCTP_MESSAGE_ERROR_MALFORMED_HEADERS,
		            "could not parse HTTP headers (error code %d)", result);

		return -1;
	} else {
		const gchar *content_length_str;
		gchar *content_length_str_end;
		gint64 content_length;
		gchar *error_msg = NULL;

		content_length_str = soup_message_headers_get_one(parsed_headers,
		                                                  "Content-Length");

		if (!content_length_str) {
			return 0;
		}

		content_length = g_ascii_strtoll(content_length_str,
		                                 &content_length_str_end, 10);

		if (content_length == 0 &&
				content_length_str_end == content_length_str) {
			error_msg = "invalid Content-Length value (%s)";
		} else if (content_length == G_MAXINT64 && errno == ERANGE) {
			error_msg = "out-of-range Content-Length value (%s)";
		} else if (content_length < 0) {
			error_msg = "negative Content-Length value (%s)";
		}

		if (error_msg) {
			g_set_error(error, HTTP_SCTP_MESSAGE_ERROR,
			            HTTP_SCTP_MESSAGE_ERROR_MALFORMED_HEADERS, error_msg,
			            content_length_str);

			return -1;
		}

		return content_length;
	}
}


/*
 * Encode the given HTTP message.
 *
 * On success a GByteArray containing the encoded message is returned,
 * while msg_end and remaining_body_len are updated with the address of
 * the first byte of the next HTTP message (or NULL if no message
 * followed the provided one) and the length of the HTTP payload that is
 * missing from the message, respectively.
 */
GByteArray*
http_sctp_message_encode(guint8 *msg, gsize msg_len, guint8 **msg_end,
                         gsize *remaining_body_len,
                         GError **error)
{
	guint16 msg_hdr[1];
	GByteArray* result;
	guint8 *headers_boundary;
	guint8 *compressed_hdrs;
	gsize headers_len;
	gsize compressed_hdrs_len = 0;
	gsize content_length;
	gsize available_body_len;

	headers_boundary = (guint8*)g_strstr_len((gchar*)msg, msg_len, "\r\n\r\n");

	if (!headers_boundary) {
		g_set_error(error, HTTP_SCTP_MESSAGE_ERROR,
		            HTTP_SCTP_MESSAGE_ERROR_TRUNCATED,
		            "the provided message doesn't contain full headers");

		return NULL;
	}

	headers_len = headers_boundary - msg;
	content_length = get_content_length((gchar*)msg, headers_len + 2, error);

	if (content_length == -1) {
		return NULL;
	}

	compressed_hdrs = encode_headers(msg, headers_len, &compressed_hdrs_len,
	                                 error);

	if (!compressed_hdrs) {
		return NULL;
	}

	available_body_len = msg_len - headers_len - 4;

	if (available_body_len > content_length) {
		available_body_len = content_length;
		*msg_end = headers_boundary + 4 + available_body_len;
	} else {
		*msg_end = NULL;
	}

	*remaining_body_len = content_length - available_body_len;
	*msg_hdr = compressed_hdrs_len;

	result = g_byte_array_sized_new(2 + compressed_hdrs_len +
	                                available_body_len);
	result = g_byte_array_append(result, (guint8*)msg_hdr, 2);
	result = g_byte_array_append(result, compressed_hdrs, compressed_hdrs_len);
	result = g_byte_array_append(result, headers_boundary + 4,
	                             available_body_len);

	return result;
}


/*
 * Decode the given HTTP-SCTP message.
 *
 * On success a GByteArray containing the decoded message is returned,
 * while msg_end and remaining_body_len are updated with the address of
 * the first byte of the next HTTP-SCTP message (or NULL if no message
 * followed the provided one) and the length of the HTTP payload that is
 * missing from the message, respectively.
 */
GByteArray*
http_sctp_message_decode(guint8 *msg, gsize msg_len, guint8 **msg_end,
                         gsize *remaining_body_len,
                         GError **error)
{
	guint16 compressed_hdrs_len;
	gboolean msg_truncated = FALSE;

	if (msg_len < 2) {
		msg_truncated = TRUE;
	} else {
		compressed_hdrs_len = *(guint16*)msg;

		if (msg_len - 2 < compressed_hdrs_len) {
			msg_truncated = TRUE;
		}
	}

	if (msg_truncated) {
		g_set_error(error, HTTP_SCTP_MESSAGE_ERROR,
		            HTTP_SCTP_MESSAGE_ERROR_TRUNCATED,
		            "truncated message detected");

		return NULL;
	} else {
		gsize in_pos = 0;
		gsize out_pos = 0;
		guint64 memlimit = DECODE_MEMORY_LIMIT;
		guint i = 2;
		GByteArray* result;
		lzma_ret return_code;

		result = g_byte_array_new();

		/*
		 * There seems to no equivalent to lzma_stream_buffer_bound for
		 * decoding, so we just try a few increasing buffer sizes in
		 * turn.
		 */
		do {
			g_byte_array_set_size(result, compressed_hdrs_len * i);
			i++;

			return_code = lzma_stream_buffer_decode(&memlimit, DECODE_FLAGS,
			                                        NULL, msg + 2, &in_pos,
			                                        compressed_hdrs_len,
			                                        result->data, &out_pos,
			                                        result->len);
		} while (return_code == LZMA_BUF_ERROR && i < 7);

		if (return_code != LZMA_OK) {
			g_set_error(error, HTTP_SCTP_MESSAGE_ERROR,
						HTTP_SCTP_MESSAGE_ERROR_LZMA,
						"lzma_stream_buffer_decode error %d\n", return_code);

			g_byte_array_free(result, TRUE);
			return NULL;
		} else {
			gsize content_length;
			gsize available_body_len;
			gchar headers_trailer[] = "\r\n\r\n";
			guint8 *body_boundary = msg + compressed_hdrs_len + 2;

			g_byte_array_set_size(result, out_pos);
			g_byte_array_append(result, (guint8*)headers_trailer, 4);

			content_length = get_content_length(result->data, result->len - 2,
			                                    error);

			if (content_length == -1) {
				return NULL;
			}

			available_body_len = msg_len - compressed_hdrs_len - 2;

			if (available_body_len > content_length) {
				available_body_len = content_length;
				*msg_end = body_boundary + content_length;
			} else {
				*msg_end = NULL;
			}

			*remaining_body_len = content_length - available_body_len;
			g_byte_array_append(result, body_boundary, available_body_len);

			return result;
		}
	}
}
