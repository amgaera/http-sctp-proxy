/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SOCKS_MESSAGE_H
#define SOCKS_MESSAGE_H

#include <gio/gio.h>


#define SOCKS_MESSAGE_ERROR (socks_message_error_quark())

GQuark socks_message_error_quark(void);

enum {
	SOCKS_MESSAGE_ERROR_TRUNCATED,
	SOCKS_MESSAGE_ERROR_MAXIMUM_LENGTH_EXCEEDED,
	SOCKS_MESSAGE_ERROR_SHORT_WRITE
};


#define SOCKS_REQUEST_CODE_CONNECT 1
#define SOCKS_REQUEST_CODE_BIND 2

/* Request granted */
#define SOCKS_REPLY_CODE_GRANTED 90
/* Request rejected or failed */
#define SOCKS_REPLY_CODE_REJECTED 91
/*
 * Request rejected because SOCKS server cannot connect to identd on the
 * client
 */
#define SOCKS_REPLY_CODE_REJECTED_IDENT_UNAVAILABLE 92
/*
 * Request rejected because the client program and identd report
 * different user-ids
 */
#define SOCKS_REPLY_CODE_REJECTED_IDENT_MISMATCH 93

/*
 * Should be at least 8 (the total size of the request numeric fields) +
 * 512 (the maximum length of an ident user ID) + 1 (the terminating
 * NUL) bytes
 */
#define SOCKS_REQUEST_MAX_SIZE 544


struct socks_request {
	guint8 version;
	guint8 code;
	guint16 dst_port;
	guint32 dst_ip;
	gchar *user_id;
};

struct socks_reply {
	guint8 version;
	guint8 code;
};


struct socks_request*
socks_request_parse(gchar *msg, gssize msg_len, GError **error);

void
socks_request_free(struct socks_request *request);

void
socks_reply_write_async(struct socks_reply *reply, GOutputStream *ostream,
                        GAsyncReadyCallback callback, gpointer user_data);

gboolean
socks_reply_write_finish(GAsyncResult *result, GError **error);

#endif
