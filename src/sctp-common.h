/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SCTP_COMMON_H
#define SCTP_COMMON_H

#include <gio/gio.h>
#include <netinet/sctp.h>


gint
socket_subscribe_sctp_events(GSocket *socket,
                             struct sctp_event_subscribe *events);

gint
socket_config_sctp_streams(GSocket *socket, guint16 num_streams);

#endif
