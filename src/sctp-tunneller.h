/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SCTP_TUNNELLER_H
#define SCTP_TUNNELLER_H

#include <gio/gio.h>
#include "http-forwarder.h"
#include "sctp-connection.h"


/*
 * Type macros
 */
#define TYPE_SCTP_TUNNELLER             (sctp_tunneller_get_type ())
#define SCTP_TUNNELLER(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SCTP_TUNNELLER, SctpTunneller))
#define IS_SCTP_TUNNELLER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SCTP_TUNNELLER))
#define SCTP_TUNNELLER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SCTP_TUNNELLER, SctpTunnellerClass))
#define IS_SCTP_TUNNELLER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SCTP_TUNNELLER))
#define SCTP_TUNNELLER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SCTP_TUNNELLER, SctpTunnellerClass))


typedef struct _SctpTunneller SctpTunneller;
typedef struct _SctpTunnellerClass SctpTunnellerClass;


struct _SctpTunneller
{
	GObject parent_instance;

	/* instance members */
	SctpConnection *connection;
	GTree *stream_load_queue;
	struct stream_info *stream_list;
	GHashTable *attached_forwarders;
};


struct _SctpTunnellerClass
{
	GObjectClass parent_class;
};


GType
sctp_tunneller_get_type(void);

#endif
