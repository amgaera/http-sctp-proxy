/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FORWARDER_CACHE_H
#define FORWARDER_CACHE_H

#include <gio/gio.h>
#include "http-forwarder.h"


/*
 * Type macros
 */
#define TYPE_FORWARDER_CACHE             (forwarder_cache_get_type ())
#define FORWARDER_CACHE(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_FORWARDER_CACHE, ForwarderCache))
#define IS_FORWARDER_CACHE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_FORWARDER_CACHE))
#define FORWARDER_CACHE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_FORWARDER_CACHE, ForwarderCacheClass))
#define IS_FORWARDER_CACHE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_FORWARDER_CACHE))
#define FORWARDER_CACHE_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_FORWARDER_CACHE, ForwarderCacheClass))


typedef struct _ForwarderCache ForwarderCache;
typedef struct _ForwarderCacheClass ForwarderCacheClass;


struct _ForwarderCache
{
	GObject parent_instance;

	/* instance members */
	GSocketClient *socket_client;
	GHashTable *forwarders;
};


struct _ForwarderCacheClass
{
	GObjectClass parent_class;
};


GType forwarder_cache_get_type(void);


void
forwarder_cache_connect_async(ForwarderCache *self, guint16 dst_port,
                              guint32 dst_ip, GAsyncReadyCallback callback,
                              gpointer user_data);


HttpForwarder*
forwarder_cache_connect_finish(ForwarderCache *self, GAsyncResult *result,
                               GError **error);

#endif
