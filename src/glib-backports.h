/*
 * Copyright (C) 2013 Artur Grunau
 * Copyright (C) 2008 Christian Kellner, Samuel Cormier-Iijima
 * Copyright (C) 2000-2001, 2006-2007 Red Hat, Inc.
 * Copyright (C) 1998 Tim Janik
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GLIB_BACKPORTS_H
#define GLIB_BACKPORTS_H

#include <gio/gio.h>


void
simple_async_report_take_gerror_in_idle (GObject *object,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data,
                                         GError *error);

gboolean
inet_address_equal (GInetAddress *address,
                    GInetAddress *other_address);

GByteArray *
byte_array_new_take (guint8 *data,
                     gsize   len);

void
queue_free_full (GQueue         *queue,
                 GDestroyNotify  free_func);

gboolean
signal_accumulator_first_wins(GSignalInvocationHint *ihint,
                              GValue *return_accu,
                              const GValue *handler_return, gpointer dummy);

#endif
