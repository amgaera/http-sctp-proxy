/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef ENCAPSULATION_H
#define ENCAPSULATION_H

#include <glib.h>


#define HTTP_SCTP_MESSAGE_ERROR (http_sctp_message_error_quark())

GQuark http_sctp_message_error_quark(void);

enum {
	HTTP_SCTP_MESSAGE_ERROR_LZMA,
	HTTP_SCTP_MESSAGE_ERROR_TRUNCATED,
	HTTP_SCTP_MESSAGE_ERROR_MALFORMED_HEADERS
};


GByteArray*
http_sctp_message_encode(guint8 *msg, gsize msg_len, guint8 **msg_end,
                         gsize *remaining_body_len,
                         GError **error);

GByteArray*
http_sctp_message_decode(guint8 *msg, gsize msg_len, guint8 **msg_end,
                         gsize *remaining_body_len,
                         GError **error);

#endif
