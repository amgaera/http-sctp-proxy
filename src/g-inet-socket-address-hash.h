/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef G_INET_SOCKET_ADDRESS_HASH_H
#define G_INET_SOCKET_ADDRESS_HASH_H

#include <gio/gio.h>


guint
g_inet_socket_address_hash_func(gconstpointer key);


gboolean
g_inet_socket_address_key_equal_func(gconstpointer a, gconstpointer b);

#endif
