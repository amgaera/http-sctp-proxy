/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * Class representing the server endpoint of an HTTP-SCTP tunnel.
 *
 * This class is used by the SCTP server to wrap the tunnelling
 * connections it receives and forward HTTP requests through them to the
 * final destination.
 */

#include <errno.h>
#include <string.h>

#include "config.h"
#include "sctp-common.h"
#include "encapsulation.h"
#include "glib-backports.h"
#include "tunnel-server-endpoint.h"


static gboolean
tunnel_server_endpoint_set_connection(HttpForwarder *self,
                                      GSocketConnection *connection,
                                      GError **error);

static void
tunnel_server_endpoint_connect_async(HttpForwarder *self,
                                     GInetSocketAddress *sock_addr,
                                     GAsyncReadyCallback callback,
                                     gpointer user_data);

static gboolean
tunnel_server_endpoint_connect_finish(HttpForwarder *self,
                                      GAsyncResult *result, GError **error);

static gboolean
tunnel_server_endpoint_is_connected(HttpForwarder *self);

static GSocketAddress*
tunnel_server_endpoint_get_remote_address(HttpForwarder *self, GError **error);

static gboolean
tunnel_server_endpoint_attach(HttpForwarder *self, HttpForwarder *other,
                              GError **error);

static gboolean
tunnel_server_endpoint_detach(HttpForwarder *self, HttpForwarder *other,
                              GError **error);

static gboolean
tunnel_server_endpoint_forward(HttpForwarder *self, HttpForwarder *other,
                               guint8 *msg, gsize msg_len, GError **error);

static void
message_received_callback(SctpConnection *connection, guint8 *msg,
                          guint64 msg_len, guint num_stream,
                          gboolean end_of_record, TunnelServerEndpoint *self);

static void
connection_closed_callback(SctpConnection *connection,
                           TunnelServerEndpoint *self);


static void
http_forwarder_interface_init(HttpForwarderInterface *iface)
{
	iface->set_connection = tunnel_server_endpoint_set_connection;
	iface->connect_async = tunnel_server_endpoint_connect_async;
	iface->connect_finish = tunnel_server_endpoint_connect_finish;
	iface->is_connected = tunnel_server_endpoint_is_connected;
	iface->get_remote_address = tunnel_server_endpoint_get_remote_address;
	iface->attach = tunnel_server_endpoint_attach;
	iface->detach = tunnel_server_endpoint_detach;
	iface->forward = tunnel_server_endpoint_forward;
}


G_DEFINE_TYPE_WITH_CODE(TunnelServerEndpoint, tunnel_server_endpoint,
                        G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(TYPE_HTTP_FORWARDER,
                                              http_forwarder_interface_init));


/*
 * State associated with each backlog message.
 */
struct msg_backlog_item {
	guint16 stream;
	guint8 *msg;
	gsize msg_len;
	gboolean end_of_record;
};


/*
 * Free state related to the provided backlog message.
 */
static void
msg_backlog_item_free(struct msg_backlog_item *item)
{
	g_free(item->msg);
	g_free(item);
}


/*
 * State associated with each attached forwarder.
 */
struct forwarder_info {
	GQueue *bound_streams;
	HttpForwarder *forwarder;
	gboolean blocked;
	guint16 cur_forwarding_stream;
	GQueue *msg_backlog;
};


/*
 * Free state related to the provided forwarder.
 */
static void
forwarder_info_free(struct forwarder_info *finfo)
{
	g_queue_free(finfo->bound_streams);
	queue_free_full(finfo->msg_backlog, (GDestroyNotify)msg_backlog_item_free);
	g_free(finfo);
}


/*
 * State associated with each stream.
 */
struct stream_info {
	HttpForwarder *cur_reading_forwarder;
};


/*
 * Compare two forwarders' load
 */
static gint
forwarder_load_compare(gconstpointer a, gconstpointer b, gpointer user_data)
{
	const struct forwarder_info *finfo_a = a;
	const struct forwarder_info *finfo_b = b;

	guint num_streams_a = g_queue_get_length(finfo_a->bound_streams);
	guint num_streams_b = g_queue_get_length(finfo_b->bound_streams);

	return num_streams_a - num_streams_b;
}


/*
 * Return the least-loaded forwarder.
 */
static HttpForwarder*
forwarder_load_get_min(GSequence *forwarder_load_queue)
{
	GSequenceIter *begin_iter;
	GSequenceIter *end_iter;

	begin_iter = g_sequence_get_begin_iter(forwarder_load_queue);
	end_iter = g_sequence_get_end_iter(forwarder_load_queue);

	if (begin_iter == end_iter) {
		return NULL;
	} else {
		struct forwarder_info *finfo;

		finfo = g_sequence_get(begin_iter);

		return finfo->forwarder;
	}
}


/*
 * Return the forwarder that should be used for the given stream.
 */
static HttpForwarder*
choose_forwarder_for_stream(GSequence *forwarder_load_queue,
                            struct stream_info *sinfo)
{
	if (sinfo->cur_reading_forwarder) {
		return sinfo->cur_reading_forwarder;
	} else {
		return forwarder_load_get_min(forwarder_load_queue);
	}
}


/*
 * Get the GSequenceIter instance pointing to the given forwarder.
 */
static GSequenceIter*
forwarder_get_iter(HttpForwarder *forwarder, GSequence *forwarder_load_queue)
{
	GSequenceIter *iter;
	GSequenceIter *end_iter;

	iter = g_sequence_get_begin_iter(forwarder_load_queue);
	end_iter = g_sequence_get_end_iter(forwarder_load_queue);

	while (1) {
		struct forwarder_info *finfo = g_sequence_get(iter);

		if (g_direct_equal(finfo->forwarder, forwarder)) {
			return iter;
		} else if (iter == end_iter) {
			return NULL;
		}

		iter = g_sequence_iter_next(iter);
	}
}


/*
 * Return the stream that should be used for the given forwarder.
 */
static guint
stream_choice_required_callback(SctpConnection *connection,
                                HttpForwarder *other,
                                TunnelServerEndpoint *self)
{
	struct forwarder_info *finfo;

	finfo = g_hash_table_lookup(self->attached_forwarders, other);

	if (g_queue_is_empty(finfo->bound_streams)) {
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
		      "unexpected HTTP message received");
	}

	return GPOINTER_TO_UINT(g_queue_pop_head(finfo->bound_streams));
}


static void
tunnel_server_endpoint_init(TunnelServerEndpoint *self)
{
	guint i;

	self->connection = NULL;

	self->stream_list = g_new(struct stream_info, NUM_STREAMS);
	self->attached_forwarders = g_hash_table_new_full(g_direct_hash,
	                                                  g_direct_equal,
	                                                  g_object_unref,
	                                                  (GDestroyNotify)forwarder_info_free);
	self->forwarder_load_queue = g_sequence_new(NULL);

	for (i = 0; i < NUM_STREAMS; i++) {
		self->stream_list[i].cur_reading_forwarder = NULL;
	}
}


static void
tunnel_server_endpoint_dispose(GObject *gobject)
{
	TunnelServerEndpoint *self = TUNNEL_SERVER_ENDPOINT(gobject);

	if (self->connection) {
		g_signal_handlers_disconnect_by_func(self->connection,
		                                     connection_closed_callback, self);
		g_object_unref(self->connection);
		self->connection = NULL;
	}

	if (self->attached_forwarders) {
		g_hash_table_destroy(self->attached_forwarders);
		self->attached_forwarders = NULL;
	}

	if (self->forwarder_load_queue) {
		g_sequence_free(self->forwarder_load_queue);
		self->attached_forwarders = NULL;
	}

	if (self->stream_list) {
		g_free(self->stream_list);
		self->stream_list = NULL;
	}

	/* Chain up to the parent class */
	G_OBJECT_CLASS(tunnel_server_endpoint_parent_class)->dispose(gobject);
}


static void
tunnel_server_endpoint_finalize(GObject *gobject)
{
	/* Chain up to the parent class */
	G_OBJECT_CLASS(tunnel_server_endpoint_parent_class)->finalize(gobject);
}


static void
tunnel_server_endpoint_class_init(TunnelServerEndpointClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

	gobject_class->dispose = tunnel_server_endpoint_dispose;
	gobject_class->finalize = tunnel_server_endpoint_finalize;
}


/*
 * Connect callbacks to the provided SCTP connection's signals.
 */
static void
connect_sctp_connection_signals(TunnelServerEndpoint *self,
                                SctpConnection *connection)
{
	g_signal_connect(connection, "connection-closed",
					 G_CALLBACK(connection_closed_callback), self);
	g_signal_connect(connection, "message-received",
					 G_CALLBACK(message_received_callback), self);
	g_signal_connect(connection, "stream-choice-required",
					 G_CALLBACK(stream_choice_required_callback), self);
}


/*
 * Set the endpoint's underlying connection.
 */
static gboolean
tunnel_server_endpoint_set_connection(HttpForwarder *self,
                                      GSocketConnection *connection,
                                      GError **error)
{
	TunnelServerEndpoint *endpoint;

	g_return_if_fail(IS_TUNNEL_SERVER_ENDPOINT(self));
	endpoint = TUNNEL_SERVER_ENDPOINT(self);

	if (endpoint->connection) {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_ALREADY_CONNECTED,
		            "endpoint already connected");
		return FALSE;
	}

	endpoint->connection = g_object_new(TYPE_SCTP_CONNECTION, NULL);
	sctp_connection_set_connection(endpoint->connection, connection);
	connect_sctp_connection_signals(endpoint, endpoint->connection);

	return TRUE;
}


/*
 * Check whether the connection was established successfully.
 */
static void
sctp_connection_connect_callback(GObject *source_object, GAsyncResult *result,
                                 gpointer user_data)
{
	GError *error = NULL;
	GSimpleAsyncResult *simple_result = user_data;
	SctpConnection *connection = SCTP_CONNECTION(source_object);

	if (!sctp_connection_connect_finish(connection, result, &error)) {
		g_simple_async_result_set_from_error(simple_result, error);
		g_error_free(error);
	}

	g_simple_async_result_complete(simple_result);
	g_object_unref(simple_result);
}


/*
 * Asynchronously connect to the specified address.
 *
 * When the asynchronous operation finishes, the provided callback will
 * be invoked. Call http_forwarder_connect_finish from it to obtain
 * the result of the operation.
 */
static void
tunnel_server_endpoint_connect_async(HttpForwarder *self,
                                     GInetSocketAddress *sock_addr,
                                     GAsyncReadyCallback callback,
                                     gpointer user_data)
{
	GSimpleAsyncResult *result;
	TunnelServerEndpoint *endpoint;

	g_return_if_fail(IS_TUNNEL_SERVER_ENDPOINT(self));
	endpoint = TUNNEL_SERVER_ENDPOINT(self);

	if (endpoint->connection) {
		g_simple_async_report_error_in_idle(G_OBJECT(self), callback,
		                                    user_data, HTTP_FORWARDER_ERROR,
		                                    HTTP_FORWARDER_ERROR_ALREADY_CONNECTED,
		                                    "endpoint already connected");
		return;
	}

	result = g_simple_async_result_new(G_OBJECT(self), callback, user_data,
									   tunnel_server_endpoint_connect_async);
	endpoint->connection = g_object_new(TYPE_SCTP_CONNECTION, NULL);

	sctp_connection_connect_async(endpoint->connection, sock_addr,
								  sctp_connection_connect_callback,
								  result);
}


/*
 * Get the result of an asynchronous connect operation.
 *
 * This function returns TRUE if the SCTP connection has been
 * successfully established, FALSE otherwise.
 */
static gboolean
tunnel_server_endpoint_connect_finish(HttpForwarder *self,
                                      GAsyncResult *result, GError **error)
{
	GSimpleAsyncResult *simple_result;

	g_return_val_if_fail(IS_TUNNEL_SERVER_ENDPOINT(self), FALSE);

	simple_result = G_SIMPLE_ASYNC_RESULT(result);

	if (g_simple_async_result_propagate_error(simple_result, error)) {
		return FALSE;
	} else {
		TunnelServerEndpoint *endpoint = TUNNEL_SERVER_ENDPOINT(self);

		connect_sctp_connection_signals(endpoint, endpoint->connection);

		return TRUE;
	}
}


/*
 * Return TRUE if the endpoint is connected, FALSE otherwise.
 */
static gboolean
tunnel_server_endpoint_is_connected(HttpForwarder *self)
{
	g_return_val_if_fail(IS_TUNNEL_SERVER_ENDPOINT(self), FALSE);

	return TUNNEL_SERVER_ENDPOINT(self)->connection != NULL;
}


/*
 * Get the endpoint's remote address.
 */
static GSocketAddress*
tunnel_server_endpoint_get_remote_address(HttpForwarder *self, GError **error)
{
	g_return_val_if_fail(IS_TUNNEL_SERVER_ENDPOINT(self), NULL);

	return sctp_connection_get_remote_address(
		TUNNEL_SERVER_ENDPOINT(self)->connection, error);
}


/*
 * Try to forward the given message through the provided forwarder.
 *
 * This function takes care not to interleave message chunks from
 * different streams on one forwarder.
 */
static gboolean
forward_message(TunnelServerEndpoint *self, struct forwarder_info *finfo,
                guint8 *msg, gsize msg_len, guint16 stream,
                gboolean end_of_record, GError **error)
{
	/* Check if the forwarder is blocked by another stream. */
	if (!finfo->blocked || finfo->cur_forwarding_stream == stream) {
		if (end_of_record) {
			GSequenceIter *iter;

			finfo->blocked = FALSE;
			g_queue_push_tail(finfo->bound_streams,
							  GUINT_TO_POINTER(stream));

			iter = forwarder_get_iter(finfo->forwarder,
			                          self->forwarder_load_queue);
			g_sequence_sort_changed(iter, forwarder_load_compare, NULL);
		} else {
			finfo->blocked = TRUE;
			finfo->cur_forwarding_stream = stream;
		}

		if (http_forwarder_forward(finfo->forwarder, HTTP_FORWARDER(self), msg,
								   msg_len, error)) {
			return TRUE;
		} else {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
				  "failed to forward HTTP message: %s", (*error)->message);
		}
	}

	return FALSE;
}


/*
 * Process the received message.
 *
 * This function decides which attached forwarder to pass the message
 * to. If the forwarding succeeds, it also tries to empty the
 * forwarder's backlog.
 */
static void
message_received_callback(SctpConnection *connection, guint8 *msg,
                          guint64 msg_len, guint num_stream,
                          gboolean end_of_record, TunnelServerEndpoint *self)
{
	GError *error = NULL;
	HttpForwarder *forwarder;
	struct stream_info *sinfo;
	guint8 *remaining_msg = msg;
	gsize remaining_msg_len = msg_len;
	gboolean free_msg = TRUE;

	sinfo = self->stream_list + num_stream;
	forwarder = choose_forwarder_for_stream(self->forwarder_load_queue, sinfo);

	if (!forwarder) {
		g_free(msg);
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING, "no forwarders attached");
	} else {
		struct forwarder_info *finfo;

		if (end_of_record) {
			sinfo->cur_reading_forwarder = NULL;
		} else {
			sinfo->cur_reading_forwarder = forwarder;
		}

		finfo = g_hash_table_lookup(self->attached_forwarders, forwarder);

		if (!forward_message(self, finfo, msg, msg_len, num_stream,
		                     end_of_record, &error)) {
			if (error) {
				g_free(msg);
				g_error_free(error);
			} else {
				struct msg_backlog_item *item;

				item = g_new(struct msg_backlog_item, 1);
				item->msg = msg;
				item->msg_len = msg_len;
				item->stream = num_stream;
				item->end_of_record = end_of_record;

				g_queue_push_tail(finfo->msg_backlog, item);
				return;
			}
		} else if (end_of_record) {
			/* Try to empty the forwarder's backlog. */
			gboolean progress_made;

			do {
				guint i;
				guint backlog_length = g_queue_get_length(finfo->msg_backlog);

				progress_made = FALSE;

				for (i = 0; i < backlog_length; i++) {
					struct msg_backlog_item *item;

					item = g_queue_pop_head(finfo->msg_backlog);

					if (forward_message(self, finfo, item->msg, item->msg_len,
					                    item->stream, item->end_of_record,
					                    &error)) {
						progress_made = TRUE;
					} else if (error) {
						progress_made = FALSE;
						g_error_free(error);
						break;
					} else {
						g_queue_push_tail(finfo->msg_backlog, item);
					}
				}
			} while (progress_made);
		}
	}
}


/*
 * Attach the given forwarder to the endpoint.
 *
 * Attached forwarders can pass messages to the endpoint. This function
 * returns TRUE if the forwarder could be attached, FALSE otherwise.
 */
gboolean
tunnel_server_endpoint_attach(HttpForwarder *self, HttpForwarder *other,
                              GError **error)
{
	TunnelServerEndpoint *endpoint;

	g_return_val_if_fail(IS_TUNNEL_SERVER_ENDPOINT(self), FALSE);
	endpoint = TUNNEL_SERVER_ENDPOINT(self);

	if (!endpoint->connection) {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_NOT_CONNECTED, "tunneller must be "
		            "connected before it can be attached to");

		return FALSE;
	}

	if (g_hash_table_lookup(endpoint->attached_forwarders, other)) {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_ALREADY_ATTACHED, "the provided "
		            "forwarder is already attached");

		return FALSE;
	} else {
		struct forwarder_info *finfo;

		finfo = g_new(struct forwarder_info, 1);
		finfo->bound_streams = g_queue_new();
		finfo->forwarder = g_object_ref(other);
		finfo->blocked = FALSE;
		finfo->cur_forwarding_stream = 0;
		finfo->msg_backlog = g_queue_new();

		g_sequence_insert_sorted(endpoint->forwarder_load_queue, finfo,
		                         forwarder_load_compare, NULL);
		g_hash_table_insert(endpoint->attached_forwarders, other, finfo);
	}

	return TRUE;
}


/*
 * Detach the given forwarder from the endpoint.
 *
 * This function returns TRUE if the forwarder could be attached,
 * FALSE otherwise.
 */
gboolean
tunnel_server_endpoint_detach(HttpForwarder *self, HttpForwarder *other,
                              GError **error)
{
	TunnelServerEndpoint *endpoint;

	g_return_val_if_fail(IS_TUNNEL_SERVER_ENDPOINT(self), FALSE);
	endpoint = TUNNEL_SERVER_ENDPOINT(self);

	if (g_hash_table_lookup(endpoint->attached_forwarders, other)) {
		GSequenceIter *iter;

		iter = forwarder_get_iter(other, endpoint->forwarder_load_queue);
		g_sequence_remove(iter);

		g_hash_table_remove(endpoint->attached_forwarders, other);

		return TRUE;
	} else {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_NOT_ATTACHED,
		            "provided forwarder wasn't attached");

		return FALSE;
	}
}


/*
 * Forward the message from the forwarder through the endpoint.
 *
 * This function returns TRUE if the message passes the endpoint's
 * validation, FALSE otherwise. There is currently no way of knowing
 * whether the message was actually successfully forwarded.
 */
static gboolean
tunnel_server_endpoint_forward(HttpForwarder *self, HttpForwarder *other,
                               guint8 *msg, gsize msg_len, GError **error)
{
	TunnelServerEndpoint *endpoint;
	struct forwarder_info *finfo;

	g_return_val_if_fail(IS_TUNNEL_SERVER_ENDPOINT(self), FALSE);
	endpoint = TUNNEL_SERVER_ENDPOINT(self);

	finfo = g_hash_table_lookup(endpoint->attached_forwarders, other);

	if (!finfo) {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_NOT_ATTACHED, "only attached "
		            "forwarders can forward messages");
		return FALSE;
	}

	if (!sctp_connection_enqueue_message(endpoint->connection, other, msg,
	                                     msg_len, error)) {
		http_forwarder_detach(self, other, NULL);

		return FALSE;
	}

	return TRUE;
}


/*
 * Clean up after the connection has been closed.
 *
 * This functions frees resources allocated to the connection and emits
 * the "connection-closed" signal.
 */
static void
connection_closed_callback(SctpConnection *connection,
                           TunnelServerEndpoint *self)
{
	gpointer forwarder;
	gpointer finfo;
	GHashTableIter iter;
	HttpForwarder *endpoint = HTTP_FORWARDER(self);
	GSequenceIter *begin_iter;
	GSequenceIter *end_iter;

	/*
	 * If the only references to the SCTP tunneller are held by its
	 * attached forwarders, it would be destroyed before this function
	 * has finished cleaning up. The following line ensures that the
	 * SCTP tunneller is kept around until we're ready to let it go.
	 */
	g_object_ref(self);
	g_signal_emit_by_name(self, "connection-closed");

	begin_iter = g_sequence_get_begin_iter(self->forwarder_load_queue);
	end_iter = g_sequence_get_end_iter(self->forwarder_load_queue);
	g_sequence_remove_range(begin_iter, end_iter);

	g_hash_table_iter_init(&iter, self->attached_forwarders);

	while (g_hash_table_iter_next(&iter, &forwarder, &finfo)) {
		GError *error = NULL;

		if (!http_forwarder_detach(HTTP_FORWARDER(forwarder), endpoint,
		                           &error)) {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
				  "tunneller detaching failed: %s\n", error->message);
			g_error_free(error);
		}

		g_hash_table_iter_remove(&iter);
	}

	g_object_unref(self->connection);
	self->connection = NULL;

	g_object_unref(self);
}
