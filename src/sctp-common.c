/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * Small SCTP-related helper functions.
 */

#include "sctp-common.h"


/*
 * Set what events the given SCTP socket should be notified about.
 *
 * This function returns 0 on success and -1 otherwise.
 */
gint
socket_subscribe_sctp_events(GSocket *socket,
                             struct sctp_event_subscribe *events)
{
	gint sock_fd = g_socket_get_fd(socket);

	return setsockopt(sock_fd, SOL_SCTP, SCTP_EVENTS, events,
	                  sizeof(struct sctp_event_subscribe));
}


/*
 * Set the socket's number of incoming and outgoing SCTP streams.
 *
 * This function returns 0 on success and -1 otherwise.
 */
gint
socket_config_sctp_streams(GSocket *socket, guint16 num_streams)
{
	gint sock_fd = g_socket_get_fd(socket);
	struct sctp_initmsg initmsg = {
		.sinit_num_ostreams = num_streams,
		.sinit_max_instreams = num_streams,
		.sinit_max_attempts = 5
	};

	return setsockopt(sock_fd, IPPROTO_SCTP, SCTP_INITMSG, &initmsg,
	                  sizeof(initmsg));
}
