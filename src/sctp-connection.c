/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * Class representing tunnelling SCTP connections.
 *
 * This class serves as the backbone of HTTP-SCTP tunnel endpoints, both
 * client- and server-side. It takes care of establishing SCTP
 * connections, properly configuring their underlying sockets,
 * encoding/decoding HTTP-SCTP messages and getting them off/on the
 * wire.
 */
#include <errno.h>
#include <string.h>

#include "config.h"
#include "marshaller.h"
#include "sctp-common.h"
#include "glib-backports.h"
#include "sctp-connection.h"
#include "encapsulation.h"


#define HTTP_REQUEST_HEADERS_MAX_SIZE 8192


G_DEFINE_TYPE(SctpConnection, sctp_connection, G_TYPE_OBJECT);


static void
connection_closed_callback(SctpConnection *self);


/*
 * State associated with each key. Keys are used to identify message
 * sources.
 */
struct associated_key_info {
	gsize remaining_body_len;
	guint16 last_stream;
};


/*
 * Free state related to the provided key.
 */
static void
associated_key_info_free(struct associated_key_info *key_info)
{
	g_free(key_info);
}


/*
 * State associated with each SCTP stream.
 */
struct stream_info {
	gsize remaining_body_len;
	gpointer cur_key;
	GQueue *bound_keys;
};


static void
sctp_connection_init(SctpConnection *self)
{
	guint i;

	self->socket = NULL;
	self->connection = NULL;
	self->input_source = NULL;
	self->output_source = NULL;
	self->connection_closed = connection_closed_callback;
	self->associated_keys = g_hash_table_new_full(g_direct_hash,
	                                              g_direct_equal, NULL,
	                                              (GDestroyNotify)associated_key_info_free);

	self->stream_list = g_new(struct stream_info, NUM_STREAMS);

	for (i = 0; i < NUM_STREAMS; i++) {
		self->stream_list[i].cur_key = NULL;
		self->stream_list[i].remaining_body_len = 0;
		self->stream_list[i].bound_keys = g_queue_new();
	}
}


static void
sctp_connection_dispose(GObject *gobject)
{
	SctpConnection *self = SCTP_CONNECTION(gobject);

	if (self->input_source) {
		g_source_destroy(self->input_source);
		g_source_unref(self->input_source);
		self->input_source = NULL;
	}

	if (self->output_source) {
		g_source_destroy(self->output_source);
		self->output_source = NULL;
	}

	if (self->socket) {
		if (self->connection) {
			g_object_unref(self->connection);
			self->connection = NULL;
		} else {
			g_object_unref(self->socket);
		}

		self->socket = NULL;
	}

	if (self->associated_keys) {
		g_hash_table_destroy(self->associated_keys);
		self->associated_keys = NULL;
	}

	if (self->stream_list) {
		guint i;

		for (i = 0; i < NUM_STREAMS; i++) {
			g_queue_free(self->stream_list[i].bound_keys);
		}

		g_free(self->stream_list);
		self->stream_list = NULL;
	}

	/* Chain up to the parent class */
	G_OBJECT_CLASS(sctp_connection_parent_class)->dispose(gobject);
}


static void
sctp_connection_finalize(GObject *gobject)
{
	/* Chain up to the parent class */
	G_OBJECT_CLASS(sctp_connection_parent_class)->finalize(gobject);
}


static void
sctp_connection_class_init(SctpConnectionClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

	/* Emitted when the underlying connection closes */
	g_signal_new("connection-closed", TYPE_SCTP_CONNECTION, G_SIGNAL_RUN_LAST,
	             G_STRUCT_OFFSET(SctpConnection, connection_closed),
	             NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	/*
	 * Emitted to inform connected handlers that a new message has been
	 * received.
	 */
	g_signal_new("message-received", TYPE_SCTP_CONNECTION, G_SIGNAL_RUN_LAST,
	             0, NULL, NULL,
	             g_cclosure_user_marshal_VOID__POINTER_UINT64_UINT_BOOLEAN,
	             G_TYPE_NONE, 4, G_TYPE_POINTER, G_TYPE_UINT64, G_TYPE_UINT,
	             G_TYPE_BOOLEAN);

	/*
	 * Emitted to query connected handlers for a stream number to use
	 * for the given key.
	 */
	g_signal_new("stream-choice-required", TYPE_SCTP_CONNECTION,
	             G_SIGNAL_RUN_LAST, 0, signal_accumulator_first_wins, NULL,
	             g_cclosure_user_marshal_UINT__POINTER, G_TYPE_UINT, 1,
	             G_TYPE_POINTER);

	gobject_class->dispose = sctp_connection_dispose;
	gobject_class->finalize = sctp_connection_finalize;
}


/*
 * State associated with each queued message.
 */
struct output_queue_item {
	gpointer key;
	GByteArray *msg;
	gsize remaining_body_len;
	guint16 stream;
};


/*
 * Free state related to the provided queued message.
 */
static void
output_queue_item_free(struct output_queue_item *output_item)
{
	g_byte_array_unref(output_item->msg);
	g_free(output_item);
}


/*
 * Global I/O context.
 */
struct socket_io_context {
	SctpConnection *self;
	guint8 *msg;
	gsize remaining_body_len;
	GQueue *output_queue;
};


/*
 * Free the global I/O context.
 */
static void
socket_io_context_free(struct socket_io_context *context)
{
	g_free(context->msg);
	queue_free_full(context->output_queue,
	                (GDestroyNotify)output_queue_item_free);
	g_free(context);
}


/*
 * Process the incoming SCTP message.
 *
 * This function parses the SCTP message, splits it into HTTP-SCTP
 * messages and emits the "message-received" signal for each one of
 * them.
 *
 * Usually several HTTP-SCTP messages wouldn't be bundled in one SCTP
 * message, but if the stream is under heavy load, it's may actually
 * happen.
 */
static void
process_incoming_message(SctpConnection *self, guint8 *msg, gsize msg_len,
                         guint16 num_stream)
{
	GError *error = NULL;
	struct stream_info *sinfo;
	guint8 *remaining_msg = msg;
	gsize remaining_msg_len = msg_len;
	/* Is the function responsible for freeing the message? */
	gboolean free_msg = TRUE;

	sinfo = self->stream_list + num_stream;

	while (remaining_msg) {
		guint8 *msg_chunk;
		gsize msg_chunk_len;
		gpointer key = g_queue_peek_head(sinfo->bound_keys);

		/*
		 * Check if the message contains contains part of the previous
		 * HTTP-SCTP message's payload.
		 */
		if (sinfo->remaining_body_len) {
			/* This branch can only be entered on the first iteration */
			g_assert(remaining_msg == msg);

			if (sinfo->remaining_body_len >= remaining_msg_len) {
				/*
				 * The whole message is just the previous HTTP-SCTP
				 * message's remaining payload. We pass it upwards as-is
				 * and don't have to worry about freeing it as that's
				 * the job of the "message-received" signal's handler.
				 */
				msg_chunk = msg;
				msg_chunk_len = msg_len;
				free_msg = FALSE;

				remaining_msg = NULL;
				sinfo->remaining_body_len -= remaining_msg_len;
			} else {
				/*
				 * Only part of the message is the previous HTTP-SCTP
				 * message's remaining payload.
				 */
				guint8 *msg_chunk = g_new(guint8, sinfo->remaining_body_len);

				msg_chunk_len = sinfo->remaining_body_len;
				memcpy(msg_chunk, remaining_msg, msg_chunk_len);

				remaining_msg = remaining_msg + msg_chunk_len;
				remaining_msg_len = remaining_msg_len - msg_chunk_len;
				sinfo->remaining_body_len = 0;
			}
		} else {
			/*
			 * Decode and process a new HTTP-SCTP message.
			 */
			GByteArray* decoded_msg;

			decoded_msg = http_sctp_message_decode(msg, msg_len,
			                                       &remaining_msg,
			                                       &sinfo->remaining_body_len,
			                                       &error);

			if (decoded_msg) {
				msg_chunk_len = decoded_msg->len;
				msg_chunk = g_byte_array_free(decoded_msg, FALSE);

				if (remaining_msg) {
					remaining_msg_len = msg_len - (remaining_msg - msg);
				}
			} else {
				g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
				      "decoding of an HTTP-SCTP message failed: %s",
				      error->message);

				g_error_free(error);
				break;
			}
		}

		g_signal_emit_by_name(self, "message-received", msg_chunk,
		                      msg_chunk_len, num_stream,
		                      sinfo->remaining_body_len == 0);

		if (!sinfo->remaining_body_len) {
			g_queue_pop_head(sinfo->bound_keys);
		}
	}

	if (free_msg) {
		g_free(msg);
	}
}


/*
 * Process the socket's output queue.
 *
 * This function tries to empty the socket's output queue, taking care
 * not to interleave message chunks from different sources on one
 * stream.
 */
static gboolean
process_output_queue(SctpConnection *self, GSocket *socket)
{
	gboolean write_occurred;
	gint sock_fd = g_socket_get_fd(socket);
	GQueue *output_queue = self->io_context->output_queue;

	do {
		guint i;
		guint queue_length = g_queue_get_length(output_queue);

		write_occurred = FALSE;

		for (i = 0; i < queue_length; i++) {
			gint bytes_sent;
			struct output_queue_item *output_item;
			struct stream_info *sinfo;

			output_item = g_queue_pop_head(output_queue);
			sinfo = self->stream_list + output_item->stream;

			if (sinfo->cur_key && sinfo->cur_key != output_item->key) {
				/* The stream is blocked by another source. */
				g_queue_push_tail(output_queue, output_item);
				continue;
			}

			bytes_sent = sctp_sendmsg(sock_fd, output_item->msg->data,
			                          output_item->msg->len, NULL, 0, 0, 0,
			                          output_item->stream, 0, 0);

			if (bytes_sent == -1) {
				if (errno != EAGAIN && errno != EWOULDBLOCK &&
						errno != EINTR) {
					output_queue_item_free(output_item);
					g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
					      "critical sctp_sendmsg error (%d), "
					      "closing connection", errno);

					return FALSE;
				} else {
					g_queue_push_head(output_queue, output_item);
					return TRUE;
				}
			}

			write_occurred = TRUE;

			if (bytes_sent < output_item->msg->len) {
				g_byte_array_remove_range(output_item->msg, 0, bytes_sent);
				sinfo->cur_key = output_item->key;
				/*
				 * TODO: if the OS uses per-stream output buffers, this
				 *       may unnecessarily block other streams
				 */
				g_queue_push_head(output_queue, output_item);
			} else {
				if (output_item->remaining_body_len) {
					sinfo->cur_key = output_item->key;
				} else {
					sinfo->cur_key = NULL;
				}

				g_queue_push_tail(sinfo->bound_keys, output_item->key);
				output_queue_item_free(output_item);
			}
		}
	} while (write_occurred);

	return TRUE;
}


/*
 * Handle changes in the socket's I/O state.
 *
 * This function gets informed when it becomes possible to read from
 * or write to the socket and call the appropriate subroutines that do
 * that. It's also notified if connection errors occur. In such cases
 * it closes the socket and emits the "connection-closed" signal.
 */
static gboolean
socket_io_callback(GSocket *socket, GIOCondition condition,
                   gpointer user_data)
{
	gboolean error_occurred = FALSE;
	struct socket_io_context *context = user_data;

	/* Writing to the socket should be possible now. */
	if (condition == G_IO_OUT) {
		error_occurred = !process_output_queue(context->self, socket);

		if (g_queue_is_empty(context->output_queue)) {
			context->self->output_source = NULL;

			return FALSE;
		}
	/* Reading from the socket should be possible now. */
	} else if (condition == G_IO_IN || condition == G_IO_PRI) {
		gint flags;
		gint bytes_read;
		gint sock_fd = g_socket_get_fd(socket);
		struct sctp_sndrcvinfo sndrcvinfo;

		bytes_read = sctp_recvmsg(sock_fd, context->msg,
		                          HTTP_REQUEST_HEADERS_MAX_SIZE, NULL, 0,
		                          &sndrcvinfo, &flags);

		if (bytes_read == -1) {
			if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR) {
				g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
				      "critical sctp_recvmsg error, closing connection");
				error_occurred = TRUE;
			}
		} else if (bytes_read == 0) {
			/* "The peer has performed an orderly shutdown" */
			error_occurred = TRUE;
		} else {
			process_incoming_message(context->self, context->msg, bytes_read,
			                         sndrcvinfo.sinfo_stream);
			context->msg = g_new(guint8, HTTP_REQUEST_HEADERS_MAX_SIZE);
		}
	} else {
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
		      "socket error condition (%d) set, closing connection",
		      condition);
		error_occurred = TRUE;
	}

	if (error_occurred) {
		g_signal_emit_by_name(context->self, "connection-closed");

		/* Remove the source */
		return FALSE;
	}

	return TRUE;
}


/*
 * Configure the connection to receive incoming data.
 *
 * This function sets up the global I/O context and creates a GSource
 * that generates notifications when the I/O state of the connection's
 * socket changes.
 */
static void
start_accepting_input(SctpConnection *self)
{
	self->io_context = g_new(struct socket_io_context, 1);
	self->io_context->self = self;
	self->io_context->output_queue = g_queue_new();
	self->io_context->remaining_body_len = 0;
	self->io_context->msg = g_new(guint8, HTTP_REQUEST_HEADERS_MAX_SIZE);

	self->input_source = g_socket_create_source(self->socket,
	                                            G_IO_IN | G_IO_PRI, NULL);
	g_source_set_callback(self->input_source, (GSourceFunc)socket_io_callback,
	                      self->io_context,
	                      (GDestroyNotify)socket_io_context_free);
	g_source_attach(self->input_source, g_main_context_get_thread_default());
}


/*
 * Sets the SctpConnection's underlying connection.
 */
void
sctp_connection_set_connection(SctpConnection *self,
                               GSocketConnection *connection)
{
	GSocket *socket;

	g_return_if_fail(IS_SCTP_CONNECTION(self));

	/*
	 * We have to keep the connection object around as in older versions
	 * of GIO destroying it would close its underlying socket, even if
	 * it was extracted and additionally ref'ed.
	 */
	self->connection = connection;
	self->socket = g_socket_connection_get_socket(connection);

	start_accepting_input(self);
}


/* State related to each socket connect operation. */
struct socket_connect_context {
	GSimpleAsyncResult *result;
	GSource *source;
};


/*
 * Check whether the socket connected successfully.
 */
static gboolean
socket_connect_callback(GSocket *socket, GIOCondition condition,
                        gpointer user_data)
{
	struct socket_connect_context *context = user_data;

	if (condition == G_IO_OUT) {
		GError *error = NULL;

		if (!g_socket_check_connect_result(socket, &error)) {
			g_simple_async_result_set_from_error(context->result, error);
			g_error_free(error);
		}
	} else {
		g_simple_async_result_set_error(context->result, HTTP_FORWARDER_ERROR,
		                                HTTP_FORWARDER_ERROR_SOCKET_ERROR,
		                                "socket_connect_callback called with "
		                                "GIOCondition = %d, expected %d",
		                                condition, G_IO_OUT);
	}

	g_simple_async_result_complete(context->result);
	g_object_unref(context->result);

	g_free(context);

	/* Remove the source */
	return FALSE;
}


/*
 * Asynchronously establish an SCTP connection to the specified address.
 *
 * When the asynchronous operation finishes, the provided callback will
 * be invoked. Call sctp_connection_connect_finish from it to obtain
 * the result of the operation.
 */
void
sctp_connection_connect_async(SctpConnection *self,
                              GInetSocketAddress *sock_addr,
                              GAsyncReadyCallback callback, gpointer user_data)
{
	GError *error = NULL;
	GSimpleAsyncResult *result;

	g_return_if_fail(IS_SCTP_CONNECTION(self));

	if (self->socket) {
		g_simple_async_report_error_in_idle(G_OBJECT(self), callback,
		                                    user_data, HTTP_FORWARDER_ERROR,
		                                    HTTP_FORWARDER_ERROR_ALREADY_CONNECTED,
		                                    "connection already connected");
		return;
	}

	self->socket = g_socket_new(G_SOCKET_FAMILY_IPV4, G_SOCKET_TYPE_STREAM,
	                            G_SOCKET_PROTOCOL_SCTP, &error);

	if (!self->socket) {
		simple_async_report_take_gerror_in_idle(G_OBJECT(self), callback,
		                                        user_data, error);
		return;
	} else {
		struct sctp_event_subscribe events = { .sctp_data_io_event = 1 };

		if (socket_config_sctp_streams(self->socket, NUM_STREAMS) != 0) {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
			      "failed to set SCTP socket options");
		}

		if (socket_subscribe_sctp_events(self->socket, &events) != 0) {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
			      "failed to set SCTP event options");
		}
	}

	g_socket_set_blocking(self->socket, FALSE);

	if (!g_socket_connect(self->socket, G_SOCKET_ADDRESS(sock_addr), NULL,
	                      &error)) {
		if (g_error_matches(error, G_IO_ERROR, G_IO_ERROR_PENDING)) {
			/*
			 * The socket started connecting. Set up a GSource that will
			 * notify us when that operation finishes.
			 */
			struct socket_connect_context *context;

			context = g_new(struct socket_connect_context, 1);
			context->source = g_socket_create_source(self->socket, G_IO_OUT,
			                                         NULL);
			context->result = g_simple_async_result_new(G_OBJECT(self),
			                                            callback, user_data,
			                                            sctp_connection_connect_async);

			g_source_attach(context->source,
			                g_main_context_get_thread_default());
			g_source_set_callback(context->source,
			                      (GSourceFunc)socket_connect_callback,
			                      context, NULL);

			g_source_unref(context->source);
			g_error_free(error);
		} else {
			simple_async_report_take_gerror_in_idle(G_OBJECT(self), callback,
			                                        user_data, error);
		}
	} else {
		/* The socket connected straight away. */
		result = g_simple_async_result_new(G_OBJECT(self), callback, user_data,
		                                   sctp_connection_connect_async);

		g_simple_async_result_complete_in_idle(result);
		g_object_unref(result);
	}
}


/*
 * Get the result of an asynchronous connect operation.
 *
 * This function returns TRUE if the SCTP connection has been
 * successfully established, FALSE otherwise.
 */
gboolean
sctp_connection_connect_finish(SctpConnection *self, GAsyncResult *result,
                               GError **error)
{
	GSimpleAsyncResult *simple_result;

	g_return_val_if_fail(IS_SCTP_CONNECTION(self), FALSE);

	simple_result = G_SIMPLE_ASYNC_RESULT(result);

	if (g_simple_async_result_propagate_error(simple_result, error)) {
		if (self->socket) {
			g_object_unref(self->socket);
			self->socket = NULL;
		}

		return FALSE;
	} else {
		start_accepting_input(self);

		return TRUE;
	}
}


/*
 * Get the connection's remote address.
 */
GSocketAddress*
sctp_connection_get_remote_address(SctpConnection *self, GError **error)
{
	g_return_val_if_fail(IS_SCTP_CONNECTION(self), NULL);

	return g_socket_get_remote_address(self->socket, error);
}


/*
 * Enqueue a message to be sent over the SCTP connection.
 *
 * This function parses the message, splits it into HTTP-SCTP messages,
 * encodes and adds them to the output queue.
 */
gboolean
sctp_connection_enqueue_message(SctpConnection *self, gpointer key,
                                guint8 *msg, gsize msg_len, GError **error)
{
	guint8 *remaining_msg = msg;
	gsize remaining_msg_len = msg_len;
	struct associated_key_info *key_info;
	/* Is the function responsible for freeing the message? */
	gboolean free_msg = TRUE;

	g_return_val_if_fail(IS_SCTP_CONNECTION(self), FALSE);

	key_info = g_hash_table_lookup(self->associated_keys, key);

	if (!key_info) {
		/* It's the first time we see this message source */
		key_info = g_new(struct associated_key_info, 1);
		key_info->remaining_body_len = 0;
		key_info->last_stream = 0;

		g_hash_table_insert(self->associated_keys, key, key_info);
	}

	while (remaining_msg) {
		struct output_queue_item *output_item;

		output_item = g_new(struct output_queue_item, 1);
		output_item->key = key;

		/*
		 * Check if the message contains part of the previous HTTP-SCTP
		 * message's payload.
		 */
		if (key_info->remaining_body_len) {
			/* This branch can only be entered on the first iteration */
			g_assert(remaining_msg == msg);
			output_item->stream = key_info->last_stream;

			if (key_info->remaining_body_len >= msg_len) {
				/*
				 * The whole message is just the previous HTTP-SCTP
				 * message's remaining payload. We add it to the output
				 * queue as-is and don't have to worry about freeing it.
				 */
				free_msg = FALSE;
				output_item->msg = byte_array_new_take(msg, msg_len);
				key_info->remaining_body_len -= msg_len;
				output_item->remaining_body_len = key_info->remaining_body_len;

				remaining_msg = NULL;
			} else {
				/*
				 * Only part of the message is the previous HTTP-SCTP
				 * message's remaining payload.
				 */
				output_item->msg = g_byte_array_sized_new(key_info->remaining_body_len);
				g_byte_array_append(output_item->msg, msg,
				                    key_info->remaining_body_len);
				output_item->remaining_body_len = 0;

				remaining_msg = msg + key_info->remaining_body_len;
				remaining_msg_len = msg_len - key_info->remaining_body_len;
				key_info->remaining_body_len = 0;
			}
		} else {
			/*
			 * Encode and process a new HTTP-SCTP message.
			 */
			guint8 *msg_end = NULL;

			output_item->msg = http_sctp_message_encode(remaining_msg,
			                                            remaining_msg_len,
			                                            &msg_end,
			                                            &key_info->remaining_body_len,
			                                            error);

			if (output_item->msg) {
				guint16 stream;

				remaining_msg = msg_end;

				if (remaining_msg) {
					remaining_msg_len = msg_len - (remaining_msg - msg);
				}

				g_signal_emit_by_name(self, "stream-choice-required", key,
				                      &stream);
				key_info->last_stream = stream;

				output_item->stream = stream;
				output_item->remaining_body_len = key_info->remaining_body_len;
			} else {
				return FALSE;
			}
		}

		g_queue_push_tail(self->io_context->output_queue, output_item);
	}

	if (free_msg) {
		g_free(msg);
	}

	/*
	 * If there is currently no source listening for output events,
	 * create one.
	 */
	if (!self->output_source) {
		self->output_source = g_socket_create_source(self->socket, G_IO_OUT,
		                                             NULL);
		g_source_set_callback(self->output_source,
		                      (GSourceFunc)socket_io_callback,
		                      self->io_context, NULL);
		g_source_attach(self->output_source,
		                g_main_context_get_thread_default());
		g_source_unref(self->output_source);
	}

	return TRUE;
}


/* Clean up after the connection has been closed. */
static void
connection_closed_callback(SctpConnection *self)
{
	if (self->connection) {
		g_object_unref(self->connection);
		self->connection = NULL;
	} else {
		g_object_unref(self->socket);
	}

	self->socket = NULL;
	g_hash_table_destroy(self->associated_keys);
}
