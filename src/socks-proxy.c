/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "config.h"
#include "forwarder-cache.h"
#include "socks-message.h"
#include "tcp-forwarder.h"


/* Check if any errors occurred when writing the SOCKS response. */
static void
socks_reply_write_callback(GObject *source_object, GAsyncResult *result,
                           gpointer user_data)
{
	GError *error = NULL;
	GSocketConnection *connection = user_data;

	if (!socks_reply_write_finish(result, &error)) {
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
		      "failed to write SOCKS response: %s", error->message);
		g_error_free(error);
	}

	g_object_unref(connection);
}


/*
 * Attach the SOCKS connection to an HTTP-SCTP tunnel.
 *
 * This callback is invoked when the forwarder cache component returns
 * an HTTP-SCTP tunnel endpoint. The TcpForwarder class is the used to
 * wrap the SOCKS connection and attach it to the tunnel.
 */
void
forwarder_cache_connect_callback(GObject *source_object, GAsyncResult *result,
                                 gpointer user_data)
{
	GError *error = NULL;
	GOutputStream *ostream;
	GSocketConnection *connection = user_data;
	struct socks_reply reply = { .version = 0 };
	HttpForwarder *server_forwarder;
	ForwarderCache *cache = FORWARDER_CACHE(source_object);

	server_forwarder = forwarder_cache_connect_finish(cache, result, &error);

	if (server_forwarder) {
		HttpForwarder *client_forwarder;

		client_forwarder = g_object_new(TYPE_TCP_FORWARDER, NULL);

		if (http_forwarder_set_connection(client_forwarder, connection,
		                                  &error)) {
			if (http_forwarder_attach(server_forwarder, client_forwarder,
									  &error)) {
				http_forwarder_attach(client_forwarder, server_forwarder,
				                      &error);
			}

			g_object_unref(client_forwarder);
		}
	}

	if (error) {
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
		      "failed to establish outgoing connection: %s", error->message);

		reply.code = SOCKS_REPLY_CODE_REJECTED;
		g_error_free(error);
	} else {
		reply.code = SOCKS_REPLY_CODE_GRANTED;
	}

	ostream = g_io_stream_get_output_stream(G_IO_STREAM(connection));

	socks_reply_write_async(&reply, ostream, socks_reply_write_callback,
	                        connection);
}


/* State related to each SOCKS request read operation. */
struct input_stream_read_context {
	GSocketConnection *connection;
	ForwarderCache *cache;
	gchar *msg;
};


/*
 * Process received SOCKS4 requests.
 *
 * This function parses the received SOCKS4 request and tries to
 * asynchronously connect to the destination host it specifies.
 */
static void
input_stream_read_callback(GObject *source_object, GAsyncResult *result,
                           gpointer user_data)
{
	gssize msg_len;
	GError *error = NULL;
	struct input_stream_read_context *context = user_data;

	msg_len = g_input_stream_read_finish((GInputStream*)source_object, result,
	                                     &error);

	if (msg_len == -1) {
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
		      "could not accept an incoming connection: %s", error->message);
		g_error_free(error);
	} else if (msg_len == 0) {
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
		      "SOCKS connection prematurely closed");
	} else {
		GError *error = NULL;
		struct socks_request *request;
		GSocketAddress *sock_addr;
		GInetAddress *inet_addr;

		request = socks_request_parse(context->msg, msg_len, &error);

		if (request) {
			if (request->version == 4) {
				/*
				 * Keep the connection alive -- the request seems to be
				 * a SOCKS4 request; we should be able to properly
				 * accept/reject it.
				 */
				g_object_ref(context->connection);

				if (request->code == SOCKS_REQUEST_CODE_CONNECT) {
					forwarder_cache_connect_async(context->cache,
					                              request->dst_port,
					                              request->dst_ip,
					                              forwarder_cache_connect_callback,
					                              context->connection);
				} else {
					GOutputStream *ostream;
					struct socks_reply reply = {
						.version = 0,
						.code = SOCKS_REPLY_CODE_REJECTED
					};

					g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
					      "SOCKS bind request rejected");

					ostream = g_io_stream_get_output_stream(
						G_IO_STREAM(context->connection));

					socks_reply_write_async(&reply, ostream,
					                        socks_reply_write_callback,
					                        context->connection);
				}
			} else {
				g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
				      "unsupported SOCKS version: %d", request->version);
			}
		} else {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING, "parse error: %s",
			      error->message);
		}
	}

	g_object_unref(context->connection);
	g_free(context->msg);
	g_free(context);
}


/*
 * Accept incoming SOCKS4 connections.
 *
 * This function tries to asynchronously receive a SOCKS4 request from
 * the connected socket.
 */
gboolean
handle_incoming_socks_connection(GSocketService *service,
                                 GSocketConnection *connection,
                                 GObject *source_object, gpointer user_data)
{
	GInputStream *istream;
	ForwarderCache *cache = user_data;
	struct input_stream_read_context *context;

	istream = g_io_stream_get_input_stream((GIOStream*)connection);

	context = g_new(struct input_stream_read_context, 1);
	context->cache = cache;
	context->connection = g_object_ref(connection);
	context->msg = g_new(gchar, SOCKS_REQUEST_MAX_SIZE);

	g_input_stream_read_async(istream, context->msg, SOCKS_REQUEST_MAX_SIZE,
	                          G_PRIORITY_DEFAULT, NULL,
	                          input_stream_read_callback, context);

	return FALSE;
}


int main()
{
	GInetAddress *inet_addr;
	GSocketAddress *sock_addr;
	GSocketService *socket_service;
	GError *error = NULL;
	ForwarderCache *cache;
	GMainLoop *loop;

	g_type_init();
	g_log_set_always_fatal(G_LOG_LEVEL_CRITICAL);

	inet_addr = g_inet_address_new_loopback(G_SOCKET_FAMILY_IPV4);
	sock_addr = g_inet_socket_address_new(inet_addr, SOCKS_PROXY_PORT);

	socket_service = g_socket_service_new();
	g_socket_listener_set_backlog((GSocketListener*)socket_service,
	                              SOCKS_PROXY_LISTEN_BACKLOG);

	if (!g_socket_listener_add_address((GSocketListener*)socket_service,
	                                   sock_addr, G_SOCKET_TYPE_STREAM,
	                                   G_SOCKET_PROTOCOL_TCP, NULL, NULL,
	                                   &error)) {
		g_critical("%s\n", error->message);
	}

	g_object_unref(sock_addr);
	g_object_unref(inet_addr);

	cache = g_object_new(TYPE_FORWARDER_CACHE, NULL);

	g_signal_connect(socket_service, "incoming",
	                 G_CALLBACK(handle_incoming_socks_connection), cache);
	g_socket_service_start(socket_service);

	loop = g_main_loop_new(NULL, FALSE);
	g_main_loop_run(loop);

	return (0);
}
