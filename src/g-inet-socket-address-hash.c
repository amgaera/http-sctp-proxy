/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * Functions required to make GInetSocketAddress instances hashable.
 */

#include "g-inet-socket-address-hash.h"


/*
 * Compute the hash of a GInetSocketAddress instance.
 */
guint
g_inet_socket_address_hash_func(gconstpointer key)
{
	GInetAddress *inet_addr;
	GInetSocketAddress *sock_addr = G_INET_SOCKET_ADDRESS(key);
	const guint8 *addr_bytes;
	const guint8 *addr_bytes_end;
	guint16 result;

	result = g_inet_socket_address_get_port(sock_addr);
	inet_addr = g_inet_socket_address_get_address(sock_addr);
	addr_bytes = g_inet_address_to_bytes(inet_addr);
	addr_bytes_end = addr_bytes + g_inet_address_get_native_size(inet_addr);

	do {
		result ^= *((guint16*)addr_bytes);
		addr_bytes += 2;
	} while (addr_bytes < addr_bytes_end);

	return result;
}


/*
 * Compare two GInetSocketAddress instances.
 */
gboolean
g_inet_socket_address_key_equal_func(gconstpointer a, gconstpointer b)
{
	GInetSocketAddress *sock_addr_a = G_INET_SOCKET_ADDRESS(a);
	GInetSocketAddress *sock_addr_b = G_INET_SOCKET_ADDRESS(b);

	if (g_inet_socket_address_get_port(sock_addr_a) ==
			g_inet_socket_address_get_port(sock_addr_b)) {
		GInetAddress *inet_addr_a;
		GInetAddress *inet_addr_b;

		inet_addr_a = g_inet_socket_address_get_address(sock_addr_a);
		inet_addr_b = g_inet_socket_address_get_address(sock_addr_b);

		return inet_address_equal(inet_addr_a, inet_addr_b);
	}

	return FALSE;
}
