/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef HTTP_FORWARDER_H
#define HTTP_FORWARDER_H

#include <gio/gio.h>


#define TYPE_HTTP_FORWARDER                 (http_forwarder_get_type ())
#define HTTP_FORWARDER(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_HTTP_FORWARDER, HttpForwarder))
#define IS_HTTP_FORWARDER(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_HTTP_FORWARDER))
#define HTTP_FORWARDER_GET_INTERFACE(inst)  (G_TYPE_INSTANCE_GET_INTERFACE ((inst), TYPE_HTTP_FORWARDER, HttpForwarderInterface))


typedef struct _HttpForwarder               HttpForwarder; /* dummy object */
typedef struct _HttpForwarderInterface      HttpForwarderInterface;


struct _HttpForwarderInterface
{
	GTypeInterface parent_iface;

	/* Public methods */
	gboolean (*set_connection) (HttpForwarder *self,
	                            GSocketConnection *connection, GError **error);
	void (*connect_async) (HttpForwarder *self, GInetSocketAddress *sock_addr,
	                       GAsyncReadyCallback callback, gpointer user_data);
	gboolean (*connect_finish) (HttpForwarder *self, GAsyncResult *result,
	                            GError **error);
	gboolean (*is_connected) (HttpForwarder *self);
	GSocketAddress* (*get_remote_address) (HttpForwarder *self, GError **error);
	gboolean (*attach) (HttpForwarder *self, HttpForwarder *other,
	                    GError **error);
	gboolean (*detach) (HttpForwarder *self, HttpForwarder *other,
	                    GError **error);
	gboolean (*forward) (HttpForwarder *self, HttpForwarder *other,
	                     guint8 *msg, gsize msg_len, GError **error);
};


#define HTTP_FORWARDER_ERROR (http_forwarder_error_quark())

GQuark http_forwarder_error_quark(void);

enum {
	HTTP_FORWARDER_ERROR_NOT_ATTACHED,
	HTTP_FORWARDER_ERROR_ALREADY_ATTACHED,
	HTTP_FORWARDER_ERROR_NOT_CONNECTED,
	HTTP_FORWARDER_ERROR_ALREADY_CONNECTED,
	HTTP_FORWARDER_ERROR_SOCKET_ERROR,
	HTTP_FORWARDER_ERROR_MALFORMED_MESSAGE
};


GType
http_forwarder_get_type(void);

gboolean
http_forwarder_set_connection(HttpForwarder *self,
                              GSocketConnection *connection, GError **error);

void
http_forwarder_connect_async(HttpForwarder *self,
                             GInetSocketAddress *sock_addr,
                             GAsyncReadyCallback callback, gpointer user_data);

gboolean
http_forwarder_connect_finish(HttpForwarder *self, GAsyncResult *result,
                              GError **error);

gboolean
http_forwarder_is_connected(HttpForwarder *self);

GSocketAddress*
http_forwarder_get_remote_address(HttpForwarder *self, GError **error);

gboolean
http_forwarder_attach(HttpForwarder *self, HttpForwarder *other,
                      GError **error);

gboolean
http_forwarder_detach(HttpForwarder *self, HttpForwarder *other,
                      GError **error);

gboolean
http_forwarder_forward(HttpForwarder *self, HttpForwarder *other, guint8 *msg,
                       gsize msg_len, GError **error);

#endif
