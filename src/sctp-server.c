/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "config.h"
#include "sctp-common.h"
#include "tcp-forwarder.h"
#include "tunnel-server-endpoint.h"


/* State related to each TCP forwarder that is or is being connected. */
struct forwarder_context {
	GSocketConnection *connection;
	HttpForwarder *tunneller;
	GInetSocketAddress *sock_addr;
};


/*
 * Handle HTTP-SCTP tunnel shutdowns.
 *
 * This callback is invoked whenever an HTTP-SCTP tunnel is shut down.
 * It then simply cleans up any state related to it.
 */
static void
tunneller_connection_closed_callback(TunnelServerEndpoint *tunneller,
                                     gpointer user_data)
{
	g_object_unref(tunneller);
}


static void
forwarder_connection_closed_callback(HttpForwarder *old_forwarder,
                                     struct forwarder_context *context);


/*
 * Attach newly connected TCP forwarders to a tunnel endpoint.
 *
 * This callback is invoked whenever a TCP forwarder finishes
 * connecting. If the forwarder connected successfully, it is attached
 * to its associated tunnel endpoint.
 */
void
http_forwarder_connect_callback(GObject *source_object, GAsyncResult *result,
                                gpointer user_data)
{
	GError *error = NULL;
	struct forwarder_context *context = user_data;
	HttpForwarder *forwarder = HTTP_FORWARDER(source_object);

	if (http_forwarder_connect_finish(forwarder, result, &error)) {
		if (!http_forwarder_is_connected(context->tunneller)) {
			/*
			 * First TCP forwarder has connected. Activate the tunnel
			 * endpoint.
			 */
			http_forwarder_set_connection(context->tunneller,
			                              context->connection, &error);
			g_signal_connect(context->tunneller, "connection-closed",
			                 G_CALLBACK(tunneller_connection_closed_callback),
			                 NULL);
		}

		if (!error && http_forwarder_attach(context->tunneller, forwarder,
		                                    &error)) {
			http_forwarder_attach(forwarder, context->tunneller, &error);
		}
	}

	if (error) {
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
		      "could not connect: %s", error->message);

		g_error_free(error);
	} else {
		g_signal_connect(forwarder, "connection-closed",
		                 G_CALLBACK(forwarder_connection_closed_callback),
		                 context);
	}

	/*
	 * The tunnel's endpoint keeps a reference to the forwarder if it
	 * successfully connected to the destination HTTP server and was
	 * attached without errors. Otherwise there's no reason to keep the
	 * forwarder around, and by dropping the only reference to it, we
	 * destroy it.
	 */
	g_object_unref(forwarder);
}


/*
 * Handle TCP forwarder disconnections.
 *
 * This callback is invoked whenever a TCP forwarder gets disconnected.
 * If the tunnel endpoint it was attached to is still connected, a new
 * TCP forwarder is created to take its place.
 */
static void
forwarder_connection_closed_callback(HttpForwarder *old_forwarder,
                                     struct forwarder_context *context)
{
	if (http_forwarder_is_connected(context->tunneller)) {
		HttpForwarder *forwarder;

		forwarder = g_object_new(TYPE_TCP_FORWARDER, NULL);
		http_forwarder_connect_async(forwarder, context->sock_addr,
		                             http_forwarder_connect_callback, context);
	} else {
		g_free(context);
	}
}


/*
 * Accept incoming tunnelling connections.
 *
 * This function uses the TunnelServerEndpoint class to convert the
 * connection into a tunnel endpoint. Subsequently, it also creates
 * regular TCP forwarders that the endpoint can use to communicate with
 * the destination HTTP server.
 */
gboolean
handle_incoming_connection(GSocketService *service,
                           GSocketConnection *connection,
                           GObject *source_object,
                           GInetSocketAddress *sock_addr)
{
	guint i;
	HttpForwarder *tunneller = g_object_new(TYPE_TUNNEL_SERVER_ENDPOINT, NULL);

	g_object_ref(connection);

	for (i = 0; i < NUM_TCP_CONNECTIONS; i++) {
		HttpForwarder *forwarder;
		struct forwarder_context *context;

		context = g_new(struct forwarder_context, 1);
		context->connection = connection;
		context->tunneller = tunneller;
		context->sock_addr = sock_addr;

		forwarder = g_object_new(TYPE_TCP_FORWARDER, NULL);
		http_forwarder_connect_async(forwarder, sock_addr,
		                             http_forwarder_connect_callback, context);
	}

	return TRUE;
}


int main()
{
	GSocket *socket;
	GInetAddress *inet_addr;
	GSocketAddress *sock_addr;
	GSocketService *socket_service;
	GError *error = NULL;
	GMainLoop *loop;

	g_type_init();
	g_log_set_always_fatal(G_LOG_LEVEL_CRITICAL);

	socket = g_socket_new(G_SOCKET_FAMILY_IPV4, G_SOCKET_TYPE_STREAM,
	                      G_SOCKET_PROTOCOL_SCTP, &error);

	if (!socket) {
		g_critical("%s", error->message);
	} else {
		struct sctp_event_subscribe events = { .sctp_data_io_event = 1 };

		if (socket_config_sctp_streams(socket, NUM_STREAMS) != 0) {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
			      "failed to set SCTP stream options");
		}

		if (socket_subscribe_sctp_events(socket, &events) != 0) {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
			      "failed to set SCTP event options");
		}
	}

	g_socket_set_blocking(socket, FALSE);

	inet_addr = g_inet_address_new_from_string(SCTP_SERVER_IP);
	sock_addr = g_inet_socket_address_new(inet_addr, SCTP_SERVER_PORT);

	if (!g_socket_bind(socket, sock_addr, TRUE, &error) ||
			!g_socket_listen(socket, &error)) {
		g_critical("%s", error->message);
	}

	g_object_unref(inet_addr);
	socket_service = g_socket_service_new();

	if (!g_socket_listener_add_socket(G_SOCKET_LISTENER(socket_service),
	                                  socket, NULL, &error)) {
		g_critical("%s", error->message);
	}

	g_signal_connect(socket_service, "incoming",
	                 G_CALLBACK(handle_incoming_connection), sock_addr);
	g_socket_service_start(socket_service);

	loop = g_main_loop_new(NULL, FALSE);
	g_main_loop_run(loop);

	return (0);
}
