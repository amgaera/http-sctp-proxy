/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef TCP_FORWARDER_H
#define TCP_FORWARDER_H

#include <gio/gio.h>
#include "http-forwarder.h"


/*
 * Type macros
 */
#define TYPE_TCP_FORWARDER             (tcp_forwarder_get_type ())
#define TCP_FORWARDER(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_TCP_FORWARDER, TcpForwarder))
#define IS_TCP_FORWARDER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_TCP_FORWARDER))
#define TCP_FORWARDER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_TCP_FORWARDER, TcpForwarderClass))
#define IS_TCP_FORWARDER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_TCP_FORWARDER))
#define TCP_FORWARDER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_TCP_FORWARDER, TcpForwarderClass))


typedef struct _TcpForwarder TcpForwarder;
typedef struct _TcpForwarderClass TcpForwarderClass;


struct _TcpForwarder
{
	GObject parent_instance;

	/* instance members */
	gboolean writing;
	GQueue *output_queue;
	GCancellable *cancellable;
	GSocketConnection *connection;
	HttpForwarder *bound_forwarder;
	struct input_stream_read_context *read_context;
	struct output_stream_write_context *write_context;
};


struct _TcpForwarderClass
{
	GObjectClass parent_class;
};


GType
tcp_forwarder_get_type(void);

#endif
