/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * Helper functions dealing with SOCKS4 messages.
 */

#include "socks-message.h"


GQuark
socks_message_error_quark(void)
{
	static GQuark quark = 0;

	if (quark == 0) {
		quark = g_quark_from_static_string("http-sctp-message-error-quark");
	}

	return quark;
}


/*
 * Parse the given SOCKS4 request.
 *
 * This function returns a pointer to struct socks_request on success,
 * NULL otherwise.
 */
struct socks_request*
socks_request_parse(gchar *msg, gssize msg_len, GError **error)
{
	struct socks_request *request;

	if (msg_len < 9) {
		g_set_error(error, SOCKS_MESSAGE_ERROR, SOCKS_MESSAGE_ERROR_TRUNCATED,
		            "SOCKS4 request must be at least 9 bytes long");

		return NULL;
	} else if (msg_len == SOCKS_REQUEST_MAX_SIZE && msg[msg_len - 1] != '\0') {
		g_set_error(error, SOCKS_MESSAGE_ERROR,
		            SOCKS_MESSAGE_ERROR_MAXIMUM_LENGTH_EXCEEDED,
		            "SOCKS4 request should not be longer than %d bytes",
		            SOCKS_REQUEST_MAX_SIZE);

		return NULL;
	}

	request = g_new(struct socks_request, 1);

	request->version = *(guint8*)msg++;
	request->code = *(guint8*)msg++;

	request->dst_port = g_ntohs(*(guint16*)msg);
	msg += 2;

	request->dst_ip = g_ntohl(*(guint32*)msg);
	msg += 4;

	request->user_id = g_new(gchar, msg_len - 8);
	g_strlcpy(request->user_id, msg, msg_len - 8);

	return request;
}


/*
 * Free the given socks_request struct.
 */
void
socks_request_free(struct socks_request *request)
{
	g_free(request->user_id);
	g_free(request);
}


/* State related to each asynchronous write of a SOCKS4 message. */
struct output_stream_write_context {
	GSimpleAsyncResult *result;
	guint8 *buffer;
};


/*
 * Check whether the SOCKS4 response has been written successfully.
 */
static void
output_stream_write_callback(GObject *source_object, GAsyncResult *result,
                             gpointer user_data)
{
	gssize bytes_written;
	GError *error = NULL;
	GSimpleAsyncResult *simple_result;
	GOutputStream *ostream = G_OUTPUT_STREAM(source_object);
	struct output_stream_write_context *context = user_data;

	simple_result = context->result;
	bytes_written = g_output_stream_write_finish(ostream, result, &error);

	if (bytes_written == -1) {
		g_simple_async_result_set_from_error(simple_result, error);
		g_error_free(error);
	} else {
		g_simple_async_result_set_op_res_gssize(simple_result, bytes_written);
	}

	g_free(context->buffer);
	g_free(context);

	g_simple_async_result_complete(simple_result);
	g_object_unref(simple_result);
}


/*
 * Asynchronously write the SOCKS4 response to the output stream.
 *
 * When the asynchronous operation finishes, the provided callback will
 * be invoked. Call socks_reply_write_finish from it to obtain
 * the result of the operation.
 */
void
socks_reply_write_async(struct socks_reply *reply, GOutputStream *ostream,
                        GAsyncReadyCallback callback, gpointer user_data)
{
	guint8 *buffer;
	GSimpleAsyncResult *result;
	struct output_stream_write_context *context;

	result = g_simple_async_result_new(NULL, callback, user_data,
	                                   socks_reply_write_async);

	/* The remaining bytes are required but ignored */
	buffer = g_new(guint8, 8);
	buffer[0] = reply->version;
	buffer[1] = reply->code;

	context = g_new(struct output_stream_write_context, 1);
	context->result = result;
	context->buffer = buffer;

	g_output_stream_write_async(ostream, buffer, 8, G_PRIORITY_DEFAULT, NULL,
	                            output_stream_write_callback, context);
}


/*
 * Get the result of an asynchronous SOCKS4 reply write operation.
 *
 * This function returns TRUE if the reply has been successfully
 * written, FALSE otherwise.
 */
gboolean
socks_reply_write_finish(GAsyncResult *result, GError **error)
{
	GSimpleAsyncResult *simple_result;

	simple_result = G_SIMPLE_ASYNC_RESULT(result);

	if (g_simple_async_result_propagate_error(simple_result, error)) {
		return FALSE;
	} else {
		gssize bytes_written;

		bytes_written = g_simple_async_result_get_op_res_gssize(simple_result);

		if (bytes_written < 8) {
			g_set_error(error, SOCKS_MESSAGE_ERROR,
			            SOCKS_MESSAGE_ERROR_SHORT_WRITE,
			            "short write detected: %zd out of 8 bytes",
			            bytes_written);

			return FALSE;
		}

		return TRUE;
	}
}
