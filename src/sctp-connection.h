/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SCTP_CONNECTION_H
#define SCTP_CONNECTION_H

#include <gio/gio.h>
#include "http-forwarder.h"


/*
 * Type macros
 */
#define TYPE_SCTP_CONNECTION             (sctp_connection_get_type ())
#define SCTP_CONNECTION(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SCTP_CONNECTION, SctpConnection))
#define IS_SCTP_CONNECTION(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SCTP_CONNECTION))
#define SCTP_CONNECTION_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SCTP_CONNECTION, SctpConnectionClass))
#define IS_SCTP_CONNECTION_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SCTP_CONNECTION))
#define SCTP_CONNECTION_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SCTP_CONNECTION, SctpConnectionClass))


typedef struct _SctpConnection SctpConnection;
typedef struct _SctpConnectionClass SctpConnectionClass;


struct _SctpConnection
{
	GObject parent_instance;

	/* instance members */
	GSocket *socket;
	GSocketConnection *connection;
	GSource *input_source;
	GSource *output_source;
	GHashTable *associated_keys;
	struct stream_info *stream_list;
	struct socket_io_context *io_context;

	/* Internal default signal handlers */
	void (*connection_closed) (SctpConnection *self);
};


struct _SctpConnectionClass
{
	GObjectClass parent_class;
};


GType
sctp_connection_get_type(void);

void
sctp_connection_set_connection(SctpConnection *self,
                               GSocketConnection *connection);

void
sctp_connection_connect_async(SctpConnection *self,
                              GInetSocketAddress *sock_addr,
                              GAsyncReadyCallback callback,
                              gpointer user_data);

gboolean
sctp_connection_connect_finish(SctpConnection *self, GAsyncResult *result,
                               GError **error);

GSocketAddress*
sctp_connection_get_remote_address(SctpConnection *self, GError **error);

gboolean
sctp_connection_enqueue_message(SctpConnection *self, gpointer key,
                                guint8 *msg, gsize msg_len, GError **error);

#endif
