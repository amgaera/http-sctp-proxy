/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * Class representing the client endpoint of an HTTP-SCTP tunnel.
 *
 * This class is used by the SOCKS proxy to establish HTTP-SCTP tunnels
 * and forward HTTP requests through them.
 */

#include <errno.h>
#include <string.h>

#include "config.h"
#include "sctp-common.h"
#include "glib-backports.h"
#include "sctp-tunneller.h"
#include "encapsulation.h"


static gboolean
sctp_tunneller_set_connection(HttpForwarder *self,
                              GSocketConnection *connection, GError **error);

static void
sctp_tunneller_connect_async(HttpForwarder *self,
                             GInetSocketAddress *sock_addr,
                             GAsyncReadyCallback callback, gpointer user_data);

static gboolean
sctp_tunneller_connect_finish(HttpForwarder *self, GAsyncResult *result,
                              GError **error);

static gboolean
sctp_tunneller_is_connected(HttpForwarder *self);

static GSocketAddress*
sctp_tunneller_get_remote_address(HttpForwarder *self, GError **error);

gboolean
sctp_tunneller_attach(HttpForwarder *self, HttpForwarder *other,
                      GError **error);

gboolean
sctp_tunneller_detach(HttpForwarder *self, HttpForwarder *other,
                      GError **error);

static gboolean
sctp_tunneller_forward(HttpForwarder *self, HttpForwarder *other, guint8 *msg,
                       gsize msg_len, GError **error);

static void
connection_closed_callback(SctpConnection *connection, SctpTunneller *self);

static void
message_received_callback(SctpConnection *connection, guint8 *msg,
                          guint64 msg_len, guint num_stream,
                          gboolean end_of_record, SctpTunneller *self);


static void
http_forwarder_interface_init(HttpForwarderInterface *iface)
{
	iface->set_connection = sctp_tunneller_set_connection;
	iface->connect_async = sctp_tunneller_connect_async;
	iface->connect_finish = sctp_tunneller_connect_finish;
	iface->is_connected = sctp_tunneller_is_connected;
	iface->get_remote_address = sctp_tunneller_get_remote_address;
	iface->attach = sctp_tunneller_attach;
	iface->detach = sctp_tunneller_detach;
	iface->forward = sctp_tunneller_forward;
}


G_DEFINE_TYPE_WITH_CODE(SctpTunneller, sctp_tunneller, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(TYPE_HTTP_FORWARDER,
                                              http_forwarder_interface_init));


/*
 * State associated with each SCTP stream.
 */
struct stream_info {
	gsize remaining_body_len;
	guint16 num_assigned_forwarders;
	HttpForwarder *cur_writing_forwarder;
	GQueue *bound_forwarders;
};


/* Compare two streams' load */
static gint
stream_load_compare(gconstpointer a, gconstpointer b,
                    struct stream_info *stream_list)
{
	gint result;
	struct stream_info *sinfo_a;
	struct stream_info *sinfo_b;
	guint16 stream_a = GPOINTER_TO_UINT(a);
	guint16 stream_b = GPOINTER_TO_UINT(b);

	sinfo_a = stream_list + stream_a;
	sinfo_b = stream_list + stream_b;

	result = sinfo_a->num_assigned_forwarders -
		sinfo_b->num_assigned_forwarders;

	return (result == 0) ? stream_a - stream_b : result;
}


/*
 * Sets data to point to the state struct of the least-loaded stream.
 */
static gboolean
stream_load_get_min(gpointer key, gpointer value, gpointer *data)
{
	*data = key;

	return TRUE;
}


/*
 * Return the number of the least-loaded stream
 *
 * The stream's load is subsequently increased by 1.
 */
static guint16
acquire_least_busy_stream(GTree *stream_load_queue)
{
	gpointer result;
	struct stream_info *sinfo;

	g_tree_foreach(stream_load_queue, (GTraverseFunc)stream_load_get_min,
	               &result);

	sinfo = g_tree_lookup(stream_load_queue, result);
	g_tree_remove(stream_load_queue, result);
	sinfo->num_assigned_forwarders++;
	g_tree_insert(stream_load_queue, result, sinfo);

	return GPOINTER_TO_UINT(result);
}


/*
 * Decrease the stream's load by 1.
 */
static void
release_stream(GTree *stream_load_queue, guint16 stream)
{
	struct stream_info *sinfo;
	gpointer key = GUINT_TO_POINTER(stream);

	sinfo = g_tree_lookup(stream_load_queue, key);
	g_tree_remove(stream_load_queue, key);
	sinfo->num_assigned_forwarders--;
	g_tree_insert(stream_load_queue, key, sinfo);
}


/*
 * Return the stream that should be used for the given forwarder.
 */
static guint
stream_choice_required_callback(SctpConnection *connection,
                                HttpForwarder *other, SctpTunneller *self)
{
	guint16 result;
	struct stream_info *sinfo;

	result = acquire_least_busy_stream(self->stream_load_queue);
	sinfo = self->stream_list + result;
	g_queue_push_tail(sinfo->bound_forwarders, other);

	return result;
}


static void
sctp_tunneller_dispose(GObject *gobject)
{
	SctpTunneller *self = SCTP_TUNNELLER(gobject);

	if (self->connection) {
		g_signal_handlers_disconnect_by_func(self->connection,
		                                     connection_closed_callback, self);
		g_object_unref(self->connection);
		self->connection = NULL;
	}

	if (self->attached_forwarders) {
		g_hash_table_destroy(self->attached_forwarders);
		self->attached_forwarders = NULL;
	}

	if (self->stream_list) {
		guint i;

		for (i = 0; i < NUM_STREAMS; i++) {
			g_queue_free(self->stream_list[i].bound_forwarders);
		}

		g_free(self->stream_list);
		self->stream_list = NULL;
	}

	if (self->stream_load_queue) {
		g_tree_destroy(self->stream_load_queue);
		self->stream_load_queue = NULL;
	}

	/* Chain up to the parent class */
	G_OBJECT_CLASS(sctp_tunneller_parent_class)->dispose(gobject);
}


static void
sctp_tunneller_finalize(GObject *gobject)
{
	/* Chain up to the parent class */
	G_OBJECT_CLASS(sctp_tunneller_parent_class)->finalize(gobject);
}


static void
sctp_tunneller_class_init(SctpTunnellerClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

	gobject_class->dispose = sctp_tunneller_dispose;
	gobject_class->finalize = sctp_tunneller_finalize;
}


/*
 * State associated with each attached forwarder.
 */
struct forwarder_info {
	gboolean data_awaited;
};


/*
 * Free state related to the provided forwarder.
 */
static void
forwarder_info_free(struct forwarder_info *finfo)
{
	g_free(finfo);
}


static void
sctp_tunneller_init(SctpTunneller *self)
{
	guint i;

	self->connection = NULL;
	self->attached_forwarders = g_hash_table_new_full(g_direct_hash,
	                                                  g_direct_equal,
	                                                  g_object_unref,
	                                                  (GDestroyNotify)forwarder_info_free);

	self->stream_list = g_new(struct stream_info, NUM_STREAMS);
	self->stream_load_queue = g_tree_new_full((GCompareDataFunc)stream_load_compare,
	                                          self->stream_list, NULL, NULL);

	for (i = 0; i < NUM_STREAMS; i++) {
		struct stream_info *sinfo = self->stream_list + i;

		sinfo->num_assigned_forwarders = 0;
		sinfo->bound_forwarders = g_queue_new();
		g_tree_insert(self->stream_load_queue, GUINT_TO_POINTER(i), sinfo);
	}
}


/*
 * Set the tunneller's underlying connection.
 */
static gboolean
sctp_tunneller_set_connection(HttpForwarder *self,
                              GSocketConnection *connection, GError **error)
{
	SctpTunneller *tunneller;

	g_return_if_fail(IS_SCTP_TUNNELLER(self));
	tunneller = SCTP_TUNNELLER(self);

	if (tunneller->connection) {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_ALREADY_CONNECTED,
		            "tunneller already connected");
		return FALSE;
	}

	tunneller->connection = g_object_new(TYPE_SCTP_CONNECTION, NULL);
	sctp_connection_set_connection(tunneller->connection, connection);

	return TRUE;
}


/*
 * Check whether the connection was established successfully.
 */
static void
sctp_connection_connect_callback(GObject *source_object, GAsyncResult *result,
                                 gpointer user_data)
{
	GError *error = NULL;
	GSimpleAsyncResult *simple_result = user_data;
	SctpConnection *connection = SCTP_CONNECTION(source_object);

	if (!sctp_connection_connect_finish(connection, result, &error)) {
		g_simple_async_result_set_from_error(simple_result, error);
		g_error_free(error);
	}

	g_simple_async_result_complete(simple_result);
	g_object_unref(simple_result);
}


/*
 * Asynchronously connect to the specified address.
 *
 * When the asynchronous operation finishes, the provided callback will
 * be invoked. Call http_forwarder_connect_finish from it to obtain
 * the result of the operation.
 */
static void
sctp_tunneller_connect_async(HttpForwarder *self,
                             GInetSocketAddress *sock_addr,
                             GAsyncReadyCallback callback, gpointer user_data)
{
	SctpTunneller *tunneller;
	GSimpleAsyncResult *result;

	g_return_if_fail(IS_SCTP_TUNNELLER(self));
	tunneller = SCTP_TUNNELLER(self);

	if (tunneller->connection) {
		g_simple_async_report_error_in_idle(G_OBJECT(self), callback,
		                                    user_data, HTTP_FORWARDER_ERROR,
		                                    HTTP_FORWARDER_ERROR_ALREADY_CONNECTED,
		                                    "tunneller already connected");
		return;
	}

	result = g_simple_async_result_new(G_OBJECT(self), callback, user_data,
	                                   sctp_tunneller_connect_async);
	tunneller->connection = g_object_new(TYPE_SCTP_CONNECTION, NULL);

	sctp_connection_connect_async(tunneller->connection, sock_addr,
								  sctp_connection_connect_callback,
								  result);
}


/*
 * Get the result of an asynchronous connect operation.
 *
 * This function returns TRUE if the SCTP connection has been
 * successfully established, FALSE otherwise.
 */
static gboolean
sctp_tunneller_connect_finish(HttpForwarder *self, GAsyncResult *result,
                              GError **error)
{
	GSimpleAsyncResult *simple_result;

	g_return_val_if_fail(IS_SCTP_TUNNELLER(self), FALSE);

	simple_result = G_SIMPLE_ASYNC_RESULT(result);

	if (g_simple_async_result_propagate_error(simple_result, error)) {
		return FALSE;
	} else {
		SctpTunneller *tunneller = SCTP_TUNNELLER(self);

		g_signal_connect(tunneller->connection, "connection-closed",
		                 G_CALLBACK(connection_closed_callback), self);
		g_signal_connect(tunneller->connection, "message-received",
		                 G_CALLBACK(message_received_callback), self);
		g_signal_connect(tunneller->connection, "stream-choice-required",
		                 G_CALLBACK(stream_choice_required_callback), self);

		return TRUE;
	}
}


/*
 * Return TRUE if the tunneller is connected, FALSE otherwise.
 */
static gboolean
sctp_tunneller_is_connected(HttpForwarder *self)
{
	g_return_val_if_fail(IS_SCTP_TUNNELLER(self), FALSE);

	return SCTP_TUNNELLER(self)->connection != NULL;
}


/*
 * Get the tunneller's remote address.
 */
static GSocketAddress*
sctp_tunneller_get_remote_address(HttpForwarder *self, GError **error)
{
	g_return_val_if_fail(IS_SCTP_TUNNELLER(self), NULL);

	return sctp_connection_get_remote_address(SCTP_TUNNELLER(self)->connection,
	                                          error);
}


/*
 * Process the received message.
 *
 * This function decides which attached forwarder to pass the message
 * to.
 */
static void
message_received_callback(SctpConnection *connection, guint8 *msg,
                          guint64 msg_len, guint num_stream,
                          gboolean end_of_record, SctpTunneller *self)
{
	struct stream_info *sinfo;
	gboolean free_msg = TRUE;
	HttpForwarder *forwarder;

	sinfo = self->stream_list + num_stream;
	forwarder = g_queue_peek_head(sinfo->bound_forwarders);

	if (!forwarder) {
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
		      "unexpected HTTP-SCTP message received");
	} else {
		GError *error = NULL;

		if (end_of_record) {
			g_queue_pop_head(sinfo->bound_forwarders);
			release_stream(self->stream_load_queue, num_stream);
		}

		if (!g_hash_table_lookup(self->attached_forwarders, forwarder)) {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING, "dropping HTTP-SCTP "
			      "message for a forwarder that is no longer attached");
		} else {
			if (!http_forwarder_forward(forwarder, HTTP_FORWARDER(self),
			                            msg, msg_len, &error)) {
				g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
				      "failed to forward an HTTP message: %s", error->message);
				g_error_free(error);
			} else {
				return;
			}
		}
	}

	g_free(msg);
}


/*
 * Attach the given forwarder to the tunneller.
 *
 * Attached forwarders can pass messages to the tunneller. This function
 * returns TRUE if the forwarder could be attached, FALSE otherwise.
 */
gboolean
sctp_tunneller_attach(HttpForwarder *self, HttpForwarder *other,
                      GError **error)
{
	SctpTunneller *tunneller;

	g_return_val_if_fail(IS_SCTP_TUNNELLER(self), FALSE);
	tunneller = SCTP_TUNNELLER(self);

	if (!tunneller->connection) {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_NOT_CONNECTED, "tunneller must be "
		            "connected before it can be attached to");

		return FALSE;
	}

	if (g_hash_table_lookup(tunneller->attached_forwarders, other)) {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_ALREADY_ATTACHED, "the provided "
		            "forwarder is already attached");

		return FALSE;
	} else {
		struct forwarder_info *finfo;

		finfo = g_new(struct forwarder_info, 1);
		finfo->data_awaited = FALSE;

		g_hash_table_insert(tunneller->attached_forwarders,
		                    g_object_ref(other), finfo);
	}

	return TRUE;
}


/*
 * Detach the given forwarder from the tunneller.
 *
 * This function returns TRUE if the forwarder could be attached,
 * FALSE otherwise.
 */
gboolean
sctp_tunneller_detach(HttpForwarder *self, HttpForwarder *other,
                      GError **error)
{
	g_return_val_if_fail(IS_SCTP_TUNNELLER(self), FALSE);

	if (g_hash_table_remove(SCTP_TUNNELLER(self)->attached_forwarders,
	                        other)) {
		return TRUE;
	} else {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_NOT_ATTACHED,
		            "provided forwarder wasn't attached");

		return FALSE;
	}
}


/*
 * Forward the message from the forwarder through the tunneller.
 *
 * This function returns TRUE if the message passes the tunneller's
 * validation, FALSE otherwise. There is currently no way of knowing
 * whether the message was actually successfully forwarded.
 */
static gboolean
sctp_tunneller_forward(HttpForwarder *self, HttpForwarder *other, guint8 *msg,
                       gsize msg_len, GError **error)
{
	SctpTunneller *tunneller;
	struct forwarder_info *finfo;

	g_return_val_if_fail(IS_SCTP_TUNNELLER(self), FALSE);
	tunneller = SCTP_TUNNELLER(self);

	finfo = g_hash_table_lookup(tunneller->attached_forwarders, other);

	if (!finfo) {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_NOT_ATTACHED, "only attached "
		            "forwarders can forward messages");
		return FALSE;
	}

	if (!sctp_connection_enqueue_message(tunneller->connection, other, msg,
	                                     msg_len, error)) {
		http_forwarder_detach(self, other, NULL);

		return FALSE;
	}

	return TRUE;
}


/*
 * Clean up after the connection has been closed.
 *
 * This functions frees resources allocated to the connection and emits
 * the "connection-closed" signal.
 */
static void
connection_closed_callback(SctpConnection *connection, SctpTunneller *self)
{
	gpointer forwarder;
	gpointer finfo;
	GHashTableIter iter;
	HttpForwarder *tunneller = HTTP_FORWARDER(self);

	/*
	 * If the only references to the SCTP tunneller are held by its
	 * attached forwarders, it would be destroyed before this function
	 * has finished cleaning up. The following line ensures that the
	 * SCTP tunneller is kept around until we're ready to let it go.
	 */
	g_object_ref(self);
	g_signal_emit_by_name(self, "connection-closed");

	g_hash_table_iter_init(&iter, self->attached_forwarders);

	while (g_hash_table_iter_next(&iter, &forwarder, &finfo)) {
		GError *error = NULL;

		if (!http_forwarder_detach(HTTP_FORWARDER(forwarder), tunneller,
		                           &error)) {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
			      "tunneller detaching failed: %s", error->message);
			g_error_free(error);
		}

		g_hash_table_iter_remove(&iter);
	}

	g_object_unref(self->connection);
	self->connection = NULL;

	g_object_unref(self);
}
