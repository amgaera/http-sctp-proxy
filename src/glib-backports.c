/*
 * Copyright (C) 2013 Artur Grunau
 * Copyright (C) 2008 Christian Kellner, Samuel Cormier-Iijima
 * Copyright (C) 2000-2001, 2006-2007 Red Hat, Inc.
 * Copyright (C) 1998 Tim Janik
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Backports of functions from newer versions of Glib
 */

#include "glib-backports.h"


GSimpleAsyncResult *
simple_async_result_new_take_error (GObject             *source_object,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data,
                                    GError              *error)
{
  GSimpleAsyncResult *simple;

  g_return_val_if_fail (!source_object || G_IS_OBJECT (source_object), NULL);

  simple = g_simple_async_result_new (source_object,
                                      callback,
                                      user_data, NULL);
  g_simple_async_result_set_from_error(simple, error);
  g_error_free(error);

  return simple;
}


void
simple_async_report_take_gerror_in_idle (GObject *object,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data,
                                         GError *error)
{
  GSimpleAsyncResult *simple;

  g_return_if_fail (!object || G_IS_OBJECT (object));
  g_return_if_fail (error != NULL);

  simple = simple_async_result_new_take_error (object,
                                               callback,
                                               user_data,
                                               error);
  g_simple_async_result_complete_in_idle (simple);
  g_object_unref (simple);
}


gboolean
inet_address_equal (GInetAddress *address,
                    GInetAddress *other_address)
{
  g_return_val_if_fail (G_IS_INET_ADDRESS (address), FALSE);
  g_return_val_if_fail (G_IS_INET_ADDRESS (other_address), FALSE);

  if (g_inet_address_get_family (address) != g_inet_address_get_family (other_address))
    return FALSE;

  if (memcmp (g_inet_address_to_bytes (address),
              g_inet_address_to_bytes (other_address),
              g_inet_address_get_native_size (address)) != 0)
    return FALSE;

  return TRUE;
}


GByteArray *
byte_array_new_take (guint8 *data,
                     gsize   len)
{
  GByteArray *array;

  array = g_byte_array_new ();

  array->data = data;
  array->len = len;

  return array;
}


void
queue_free_full (GQueue         *queue,
                 GDestroyNotify  free_func)
{
  g_queue_foreach (queue, (GFunc) free_func, NULL);
  g_queue_free (queue);
}


gboolean
signal_accumulator_first_wins(GSignalInvocationHint *ihint,
                              GValue *return_accu,
                              const GValue *handler_return, gpointer dummy)
{
	g_value_copy(handler_return, return_accu);
	return FALSE;
}
