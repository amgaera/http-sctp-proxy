/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * Class representing a forwarder cache.
 *
 * When a forwarder is requested from an ForwarderCache, the proxy will first
 * try to return one from its cache. If it has no forwarders connected
 * to the specified address, it will create, connect, and return a new
 * forwarder.
 */

#include "glib-backports.h"
#include "sctp-tunneller.h"
#include "forwarder-cache.h"
#include "g-inet-socket-address-hash.h"


#define HTTP_REQUEST_HEADERS_MAX_SIZE 8192


G_DEFINE_TYPE(ForwarderCache, forwarder_cache, G_TYPE_OBJECT);


static void
forwarder_cache_dispose(GObject *gobject)
{
	ForwarderCache *self = FORWARDER_CACHE(gobject);

	if (self->socket_client) {
		g_object_unref(self->socket_client);
		self->socket_client = NULL;
	}

	if (self->forwarders) {
		g_hash_table_destroy(self->forwarders);
		self->forwarders = NULL;
	}

	/* Chain up to the parent class */
	G_OBJECT_CLASS(forwarder_cache_parent_class)->dispose(gobject);
}


static void
forwarder_cache_finalize(GObject *gobject)
{
	/* Chain up to the parent class */
	G_OBJECT_CLASS(forwarder_cache_parent_class)->finalize(gobject);
}


static void
forwarder_cache_class_init(ForwarderCacheClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

	gobject_class->dispose = forwarder_cache_dispose;
	gobject_class->finalize = forwarder_cache_finalize;
}


/*
 * State related to each entry in the proxy's forwarder table.
 */
struct forwarder_cache_entry {
	GQueue *waiting_queue;
	HttpForwarder *forwarder;
	gboolean connected;
};


/*
 * Free the state related to the given entry from the forwarder table.
 */
static void
forwarder_cache_entry_free(struct forwarder_cache_entry *entry)
{
	if (entry->waiting_queue) {
		queue_free_full(entry->waiting_queue, g_object_unref);
	}
	g_free(entry);
}


static void
forwarder_cache_init(ForwarderCache *self)
{
	self->socket_client = g_socket_client_new();
	self->forwarders = g_hash_table_new_full(g_inet_socket_address_hash_func,
		g_inet_socket_address_key_equal_func, g_object_unref,
		(GDestroyNotify)forwarder_cache_entry_free);
}


/*
 * Call g_simple_async_result_complete on the given GSimpleAsyncResult
 * instance.
 */
static void
complete_simple_async_result(GSimpleAsyncResult *result, gpointer user_data)
{
	g_simple_async_result_complete(result);
	g_object_unref(result);
}


/*
 * Check whether the forwarder connected successfully.
 */
static void
http_forwarder_connect_callback(GObject *source_object, GAsyncResult *result,
                                gpointer user_data)
{
	GError *error = NULL;
	struct forwarder_cache_entry *entry = user_data;
	HttpForwarder *forwarder = HTTP_FORWARDER(source_object);

	if (!http_forwarder_connect_finish(forwarder, result, &error)) {
		g_queue_foreach(entry->waiting_queue,
		                (GFunc)g_simple_async_result_set_from_error, error);
		g_error_free(error);
	} else {
		entry->connected = TRUE;
	}

	g_queue_foreach(entry->waiting_queue, (GFunc)complete_simple_async_result,
	                NULL);
	g_queue_free(entry->waiting_queue);
}


/*
 * Remove the disconnected forwarder from the cache.
 */
static void
forwarder_connection_closed(HttpForwarder *forwarder, gpointer user_data)
{
	GError *error = NULL;
	GSocketAddress *sock_addr;
	ForwarderCache *self = user_data;

	sock_addr = http_forwarder_get_remote_address(forwarder, &error);

	if (sock_addr) {
		g_hash_table_remove(self->forwarders, sock_addr);
	} else {
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
		      "failed to retrieve forwarder's remote address: %s",
		      error->message);
		g_error_free(error);
	}
}


/*
 * Asynchronously establish a connection to the specified address.
 *
 * When the asynchronous operation finishes, the provided callback will
 * be invoked. Call forwarder_cache_connect_finish from it to obtain the
 * result of the operation.
 */
void
forwarder_cache_connect_async(ForwarderCache *self, guint16 dst_port,
                              guint32 dst_ip, GAsyncReadyCallback callback,
                              gpointer user_data)
{
	GInetAddress *inet_addr;
	GSocketAddress *sock_addr;
	GSimpleAsyncResult *simple_result;
	struct forwarder_cache_entry *entry;

	g_return_if_fail(IS_FORWARDER_CACHE(self));

	if (!self->socket_client) {
		return;
	}

	dst_ip = g_htonl(dst_ip);
	inet_addr = g_inet_address_new_from_bytes((guint8*)&dst_ip,
	                                          G_SOCKET_FAMILY_IPV4);
	sock_addr = g_inet_socket_address_new(inet_addr, dst_port);

	simple_result = g_simple_async_result_new(G_OBJECT(self), callback,
	                                          user_data,
	                                          forwarder_cache_connect_async);

	/*
	 * Check if there is already a cached forwarder connected to the
	 * given address.
	 */
	entry = g_hash_table_lookup(self->forwarders, sock_addr);

	if (!entry) {
		HttpForwarder *forwarder = g_object_new(TYPE_SCTP_TUNNELLER, NULL);

		entry = g_new(struct forwarder_cache_entry, 1);
		entry->connected = FALSE;
		entry->forwarder = forwarder;
		entry->waiting_queue = g_queue_new();

		g_hash_table_insert(self->forwarders, sock_addr, entry);

		http_forwarder_connect_async(forwarder,
		                             G_INET_SOCKET_ADDRESS(sock_addr),
		                             http_forwarder_connect_callback,
		                             entry);
	} else {
		g_object_unref(sock_addr);
	}

	g_simple_async_result_set_op_res_gpointer(simple_result,
		g_object_ref(entry->forwarder), g_object_unref);

	if (entry->connected) {
		g_simple_async_result_complete_in_idle(simple_result);
		g_object_unref(simple_result);
	} else {
		g_queue_push_tail(entry->waiting_queue, simple_result);
	}

	g_object_unref(inet_addr);
}


/*
 * Get the result of an asynchronous connect operation.
 *
 * This function returns TRUE if the connection has been successfully
 * established, FALSE otherwise.
 */
HttpForwarder*
forwarder_cache_connect_finish(ForwarderCache *self, GAsyncResult *result,
                               GError **error)
{
	GSimpleAsyncResult *simple_result;

	g_return_val_if_fail(IS_FORWARDER_CACHE(self), NULL);

	simple_result = G_SIMPLE_ASYNC_RESULT(result);

	if (g_simple_async_result_propagate_error(simple_result, error)) {
		return NULL;
	} else {
		return HTTP_FORWARDER(
			g_simple_async_result_get_op_res_gpointer(simple_result));
	}
}
