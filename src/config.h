/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#define NUM_STREAMS 64
#define SOCKS_PROXY_PORT 1519
#define SOCKS_PROXY_LISTEN_BACKLOG 50
#define NUM_TCP_CONNECTIONS 5
#define SCTP_SERVER_IP "127.0.0.1"
#define SCTP_SERVER_PORT 8080
