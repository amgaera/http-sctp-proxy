/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * Class representing a simple TCP-based HTTP forwarding endpoint.
 *
 * This class is used to wrap the SOCKS proxy's incoming connections and
 * the SCTP server's outgoing ones. It provides the very standard
 * HTTP-over-TCP transport.
 */

#include "tcp-forwarder.h"
#include "glib-backports.h"


#define REQUEST_BUFFER_SIZE 8192


static gboolean
tcp_forwarder_set_connection(HttpForwarder *self,
                             GSocketConnection *connection, GError **error);

static void
tcp_forwarder_connect_async(TcpForwarder *self, GInetSocketAddress *sock_addr,
                            GAsyncReadyCallback callback, gpointer user_data);

static gboolean
tcp_forwarder_connect_finish(TcpForwarder *self, GAsyncResult *result,
                             GError **error);

static GSocketAddress*
tcp_forwarder_get_remote_address(HttpForwarder *self, GError **error);

gboolean
tcp_forwarder_attach(HttpForwarder *self, HttpForwarder *other,
                     GError **error);

gboolean
tcp_forwarder_detach(HttpForwarder *self, HttpForwarder *other,
                     GError **error);

static gboolean
tcp_forwarder_forward(HttpForwarder *self, HttpForwarder *other, guint8 *msg,
                      gsize msg_len, GError **error);

static void
tcp_forwarder_connection_closed(TcpForwarder *self);


static void
http_forwarder_interface_init(HttpForwarderInterface *iface)
{
	iface->set_connection = tcp_forwarder_set_connection;
	iface->connect_async = tcp_forwarder_connect_async;
	iface->connect_finish = tcp_forwarder_connect_finish;
	iface->get_remote_address = tcp_forwarder_get_remote_address;
	iface->attach = tcp_forwarder_attach;
	iface->detach = tcp_forwarder_detach;
	iface->forward = tcp_forwarder_forward;
}


G_DEFINE_TYPE_WITH_CODE(TcpForwarder, tcp_forwarder, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(TYPE_HTTP_FORWARDER,
                                              http_forwarder_interface_init));


/* State related to read operations */
struct input_stream_read_context {
	TcpForwarder *self;
	guint8 *request_buffer;
	gboolean object_exists;
};


/* State related to write operations */
struct output_stream_write_context {
	GByteArray *msg;
	TcpForwarder *self;
	gboolean object_exists;
};


static void
tcp_forwarder_dispose(GObject *gobject)
{
	TcpForwarder *self = TCP_FORWARDER(gobject);

	if (self->cancellable) {
		if (self->read_context) {
			self->read_context->object_exists = FALSE;
			self->read_context = NULL;
		}

		if (self->write_context) {
			if (self->writing) {
				self->write_context->object_exists = FALSE;
			} else {
				g_free(self->write_context);
			}

			self->write_context = NULL;
		}

		if (self->connection) {
			g_cancellable_cancel(self->cancellable);
			g_object_unref(self->connection);
			self->connection = NULL;
		}

		if (self->bound_forwarder) {
			g_object_unref(self->bound_forwarder);
			self->bound_forwarder = NULL;
		}

		g_object_unref(self->cancellable);
		self->cancellable = NULL;
	}

	/* Chain up to the parent class */
	G_OBJECT_CLASS(tcp_forwarder_parent_class)->dispose(gobject);
}


static void
tcp_forwarder_finalize(GObject *gobject)
{
	/* Chain up to the parent class */
	G_OBJECT_CLASS(tcp_forwarder_parent_class)->finalize(gobject);
}


static void
tcp_forwarder_class_init(TcpForwarderClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

	gobject_class->dispose = tcp_forwarder_dispose;
	gobject_class->finalize = tcp_forwarder_finalize;
}


static void
tcp_forwarder_init(TcpForwarder *self)
{
	self->writing = FALSE;
	self->output_queue = g_queue_new();
	self->connection = NULL;
	self->bound_forwarder = NULL;
	self->cancellable = g_cancellable_new();
	self->read_context = NULL;
	self->write_context = NULL;
}


/* Set the forwarder's underlying connection. */
static gboolean
tcp_forwarder_set_connection(HttpForwarder *self,
                             GSocketConnection *connection, GError **error)
{
	g_return_if_fail(IS_TCP_FORWARDER(self));

	TCP_FORWARDER(self)->connection = g_object_ref(connection);
}


/*
 * Check whether the connection was established successfully.
 */
static void
socket_client_connect_callback(GObject *source_object, GAsyncResult *result,
                               gpointer user_data)
{
	GError *error = NULL;
	GSocketConnection *connection;
	GSimpleAsyncResult *simple_result = user_data;
	GSocketClient *socket_client = G_SOCKET_CLIENT(source_object);

	connection = g_socket_client_connect_finish(socket_client,
	                                            result, &error);

	if (!connection) {
		g_simple_async_result_set_from_error(simple_result, error);
		g_error_free(error);
	} else {
		g_simple_async_result_set_op_res_gpointer(simple_result,
		                                          g_object_ref(connection),
		                                          g_object_unref);
	}

	g_simple_async_result_complete(simple_result);
	g_object_unref(simple_result);
	g_object_unref(socket_client);
}


/*
 * Asynchronously connect to the specified address.
 *
 * When the asynchronous operation finishes, the provided callback will
 * be invoked. Call http_forwarder_connect_finish from it to obtain
 * the result of the operation.
 */
static void
tcp_forwarder_connect_async(TcpForwarder *self, GInetSocketAddress *sock_addr,
                            GAsyncReadyCallback callback, gpointer user_data)
{
	GSocketClient *socket_client;
	GSimpleAsyncResult *simple_result;

	g_return_if_fail(IS_TCP_FORWARDER(self));

	simple_result = g_simple_async_result_new(G_OBJECT(self), callback,
	                                          user_data,
	                                          tcp_forwarder_connect_async);

	socket_client = g_socket_client_new();
	g_socket_client_connect_async(socket_client,
								  G_SOCKET_CONNECTABLE(sock_addr), NULL,
								  socket_client_connect_callback,
								  simple_result);
}


/*
 * Get the result of an asynchronous connect operation.
 *
 * This function returns TRUE if the TCP connection has been
 * successfully established, FALSE otherwise.
 */
static gboolean
tcp_forwarder_connect_finish(TcpForwarder *self, GAsyncResult *result,
                             GError **error)
{
	GSimpleAsyncResult *simple_result;

	g_return_val_if_fail(IS_TCP_FORWARDER(self), FALSE);

	simple_result = G_SIMPLE_ASYNC_RESULT(result);

	if (g_simple_async_result_propagate_error(simple_result, error)) {
		return FALSE;
	} else {
		self->connection = G_SOCKET_CONNECTION(
			g_simple_async_result_get_op_res_gpointer(simple_result));

		return TRUE;
	}
}


/*
 * Get the forwarder's remote address.
 */
static GSocketAddress*
tcp_forwarder_get_remote_address(HttpForwarder *self, GError **error)
{
	g_return_val_if_fail(IS_TCP_FORWARDER(self), NULL);

	return g_socket_connection_get_remote_address(TCP_FORWARDER(self)->connection,
	                                              error);
}


static void
input_stream_read_callback(GObject *source_object, GAsyncResult *result,
                           gpointer user_data);


/*
 * Initiate an asynchronous read from the input stream.
 */
static void
input_stream_read(GInputStream *istream,
                  struct input_stream_read_context *context)
{
	context->request_buffer = g_new(guint8, REQUEST_BUFFER_SIZE);

	g_input_stream_read_async(istream, context->request_buffer,
							  REQUEST_BUFFER_SIZE, G_PRIORITY_DEFAULT,
							  context->self->cancellable,
	                          input_stream_read_callback, context);
}


/*
 * Handle the received message.
 *
 * This function checks whether the message was read without errors,
 * and if so, forwards it further.
 */
static void
input_stream_read_callback(GObject *source_object, GAsyncResult *result,
                           gpointer user_data)
{
	gssize msg_len;
	GError *error = NULL;
	GInputStream *istream = G_INPUT_STREAM(source_object);
	struct input_stream_read_context *context = user_data;

	msg_len = g_input_stream_read_finish(istream, result, &error);

	if (msg_len == -1) {
		if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED)) {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
				  "error while receiving an HTTP request: %s",
			      error->message);
		}

		g_error_free(error);
	} else if (msg_len == 0) {
		/* The underlying connection has been closed. */
		if (context->object_exists) {
			tcp_forwarder_connection_closed(context->self);
		}

		g_free(context->request_buffer);
		g_free(context);
	} else {
		if (!context->self->bound_forwarder) {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
			      "unexpected HTTP message received");
		} else {
			if (!http_forwarder_forward(context->self->bound_forwarder,
			                            HTTP_FORWARDER(context->self),
			                            context->request_buffer, msg_len,
			                            &error)) {
				g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
				      "failed to forward an HTTP message: %s", error->message);
			}

			input_stream_read(istream, context);
		}
	}
}


/*
 * Attach the second forwarder to the first one.
 *
 * Attached forwarders can pass messages to each other. This function
 * returns TRUE if the forwarder could be attached, FALSE otherwise.
 */
gboolean
tcp_forwarder_attach(HttpForwarder *self, HttpForwarder *other, GError **error)
{
	TcpForwarder *forwarder;

	g_return_val_if_fail(IS_TCP_FORWARDER(self), FALSE);
	forwarder = TCP_FORWARDER(self);

	if (forwarder->connection) {
		GIOStream *iostream;
		GInputStream *istream;

		forwarder->bound_forwarder = g_object_ref(other);
		forwarder->read_context = g_new(struct input_stream_read_context, 1);
		forwarder->read_context->self = forwarder;
		forwarder->read_context->object_exists = TRUE;

		iostream = G_IO_STREAM(forwarder->connection);
		istream = g_io_stream_get_input_stream(iostream);
		input_stream_read(istream, forwarder->read_context);

		return TRUE;
	} else {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_NOT_CONNECTED, "forwarder must be "
		            "connected before it can be attached to");

		return FALSE;
	}
}


/*
 * Detach the second forwarder from the first one.
 *
 * This function returns TRUE if the forwarder could be attached,
 * FALSE otherwise.
 */
gboolean
tcp_forwarder_detach(HttpForwarder *self, HttpForwarder *other, GError **error)
{
	g_return_val_if_fail(IS_TCP_FORWARDER(self), FALSE);

	if (TCP_FORWARDER(self)->bound_forwarder == other) {
		g_object_unref(other);
		TCP_FORWARDER(self)->bound_forwarder = NULL;

		return TRUE;
	} else {
		g_set_error(error, HTTP_FORWARDER_ERROR,
		            HTTP_FORWARDER_ERROR_NOT_ATTACHED,
		            "the provided forwarders are not attached");

		return FALSE;
	}
}


static void
output_stream_write_callback(GObject *source_object, GAsyncResult *result,
                             gpointer user_data);


/*
 * Initiate an asynchronous write to the output stream.
 */
static void
output_stream_write(TcpForwarder *self, GOutputStream *ostream,
                    GByteArray *msg)
{
	self->write_context->msg = msg;

	g_output_stream_write_async(ostream, msg->data, msg->len,
	                            G_PRIORITY_DEFAULT, self->cancellable,
	                            output_stream_write_callback,
	                            self->write_context);
}


/*
 * Check whether the message has been written successfully.
 *
 * This function checks whether any errors occurred during the write,
 * and if not, processes the next message from the output queue.
 */
static void
output_stream_write_callback(GObject *source_object, GAsyncResult *result,
                             gpointer user_data)
{
	gssize bytes_written;
	GError *error = NULL;
	struct output_stream_write_context *context = user_data;
	GOutputStream *ostream = G_OUTPUT_STREAM(source_object);
	GByteArray *msg;

	bytes_written = g_output_stream_write_finish(ostream, result, &error);

	if (bytes_written == -1) {
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
		      "error while writing an HTTP request: %s", error->message);
		g_error_free(error);
	} else if (bytes_written < context->msg->len) {
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
		      "short write detected: %zd out of %d bytes", bytes_written,
		      context->msg->len);
	}

	g_byte_array_unref(context->msg);

	/* Check if the forwarder still exists */
	if (context->object_exists) {
		msg = g_queue_pop_head(context->self->output_queue);

		if (msg) {
			output_stream_write(context->self, ostream, msg);
		} else {
			context->self->writing = FALSE;
		}
	} else {
		g_free(context);
	}
}


/*
 * Forward the message from the second forwarder through the first one.
 *
 * This function returns TRUE if the message passes the first
 * forwarder's validation, FALSE otherwise. There is currently no way
 * of knowing whether the message was actually successfully forwarded.
 */
static gboolean
tcp_forwarder_forward(HttpForwarder *self, HttpForwarder *other, guint8 *msg,
                      gsize msg_len, GError **error)
{
	GByteArray *boxed_msg;
	TcpForwarder *forwarder;

	g_return_val_if_fail(IS_TCP_FORWARDER(self), FALSE);
	forwarder = TCP_FORWARDER(self);

	boxed_msg = byte_array_new_take(msg, msg_len);

	/*
	 * We'd get the "Stream has outstanding operation" error if we tried
	 * to start a new asynchronous write operation while another one was
	 * still active. That's why if the forwarder is currently writing,
	 * we add the message to its output queue to be processed when the
	 * current asynchronous write finishes.
	 */
	if (forwarder->writing) {
		g_queue_push_tail(forwarder->output_queue, boxed_msg);
	} else {
		GIOStream *iostream;
		GOutputStream *ostream;

		if (!forwarder->write_context) {
			forwarder->write_context = g_new(struct output_stream_write_context,
			                                 1);
			forwarder->write_context->self = forwarder;
			forwarder->write_context->object_exists = TRUE;
		}

		iostream = G_IO_STREAM(forwarder->connection);
		ostream = g_io_stream_get_output_stream(iostream);

		forwarder->writing = TRUE;
		output_stream_write(forwarder, ostream, boxed_msg);
	}

	return TRUE;
}


/*
 * Clean up after the connection has been closed.
 *
 * This functions frees resources allocated to the connection and emits
 * the "connection-closed" signal.
 */
static void
tcp_forwarder_connection_closed(TcpForwarder *self)
{
	/*
	 * If the bound forwarder we're about to detach from holds the
	 * last reference to the TCP forwarder, the latter would be
	 * destroyed before this function has finished cleaning up.
	 * The following line ensures that the TCP forwarder is kept
	 * around until we're ready to let it go.
	 */
	g_object_ref(self);
	g_signal_emit_by_name(self, "connection-closed");

	if (self->bound_forwarder) {
		GError *error = NULL;

		if (!http_forwarder_detach(self->bound_forwarder, HTTP_FORWARDER(self),
		                           &error)) {
			g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING,
			      "forwarder detaching failed: %s", error->message);
			g_error_free(error);
		}

		g_object_unref(self->bound_forwarder);
		self->bound_forwarder = NULL;
	}

	g_object_unref(self->connection);
	self->connection = NULL;

	g_object_unref(self);
}
