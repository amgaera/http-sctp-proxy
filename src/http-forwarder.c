/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * Interface for HTTP forwarding endpoints.
 */

#include "http-forwarder.h"


G_DEFINE_INTERFACE(HttpForwarder, http_forwarder, 0);


GQuark
http_forwarder_error_quark(void)
{
	static GQuark quark = 0;

	if (quark == 0) {
		quark = g_quark_from_static_string("http-forwarder-error-quark");
	}

	return quark;
}


static void
http_forwarder_default_init(HttpForwarderInterface *iface)
{
	/* Emitted when the forwarder's underlying connection closes */
	g_signal_new("connection-closed", G_TYPE_FROM_INTERFACE(iface),
	             G_SIGNAL_RUN_LAST, 0,
	             NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}


/* Set the forwarder's underlying connection. */
gboolean
http_forwarder_set_connection(HttpForwarder *self,
                              GSocketConnection *connection, GError **error)
{
	g_return_val_if_fail(IS_HTTP_FORWARDER(self), FALSE);

	HTTP_FORWARDER_GET_INTERFACE(self)->set_connection(self, connection,
	                                                   error);
}


/*
 * Asynchronously connect to the specified address.
 *
 * When the asynchronous operation finishes, the provided callback will
 * be invoked. Call http_forwarder_connect_finish from it to obtain
 * the result of the operation.
 */
void
http_forwarder_connect_async(HttpForwarder *self,
                             GInetSocketAddress *sock_addr,
                             GAsyncReadyCallback callback, gpointer user_data)
{
	g_return_if_fail(IS_HTTP_FORWARDER(self));

	HTTP_FORWARDER_GET_INTERFACE(self)->connect_async(self, sock_addr,
	                                                  callback, user_data);
}


/*
 * Get the result of an asynchronous connect operation.
 *
 * This function returns TRUE if the connection has been successfully
 * established, FALSE otherwise.
 */
gboolean
http_forwarder_connect_finish(HttpForwarder *self, GAsyncResult *result,
                              GError **error)
{
	g_return_val_if_fail(IS_HTTP_FORWARDER(self), FALSE);

	HTTP_FORWARDER_GET_INTERFACE(self)->connect_finish(self, result, error);
}


/* Return TRUE if the forwarder is connected, FALSE otherwise. */
gboolean
http_forwarder_is_connected(HttpForwarder *self)
{
	g_return_val_if_fail(IS_HTTP_FORWARDER(self), FALSE);

	HTTP_FORWARDER_GET_INTERFACE(self)->is_connected(self);
}


/*
 * Get the forwarder's remote address.
 */
GSocketAddress*
http_forwarder_get_remote_address(HttpForwarder *self, GError **error)
{
	g_return_val_if_fail(IS_HTTP_FORWARDER(self), NULL);

	HTTP_FORWARDER_GET_INTERFACE(self)->get_remote_address(self, error);
}


/*
 * Attach the second forwarder to the first one.
 *
 * Attached forwarders can pass messages to each other. This function
 * returns TRUE if the forwarder could be attached, FALSE otherwise.
 */
gboolean
http_forwarder_attach(HttpForwarder *self, HttpForwarder *other,
                      GError **error)
{
	g_return_val_if_fail(IS_HTTP_FORWARDER(self), FALSE);

	HTTP_FORWARDER_GET_INTERFACE(self)->attach(self, other, error);
}


/*
 * Detach the second forwarder from the first one.
 *
 * This function returns TRUE if the forwarder could be attached,
 * FALSE otherwise.
 */
gboolean
http_forwarder_detach(HttpForwarder *self, HttpForwarder *other,
                      GError **error)
{
	g_return_val_if_fail(IS_HTTP_FORWARDER(self), FALSE);

	HTTP_FORWARDER_GET_INTERFACE(self)->detach(self, other, error);
}


/*
 * Forward the message from the second forwarder through the first one.
 *
 * This function returns TRUE if the message passes the first
 * forwarder's validation, FALSE otherwise. There is currently no way
 * of knowing whether the message was actually successfully forwarded.
 */
gboolean
http_forwarder_forward(HttpForwarder *self, HttpForwarder *other, guint8 *msg,
                       gsize msg_len, GError **error)
{
	g_return_val_if_fail(IS_HTTP_FORWARDER(self), FALSE);

	HTTP_FORWARDER_GET_INTERFACE(self)->forward(self, other, msg, msg_len,
	                                            error);
}
