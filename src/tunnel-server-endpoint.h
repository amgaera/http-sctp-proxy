/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef TUNNEL_SERVER_ENDPOINT_H
#define TUNNEL_SERVER_ENDPOINT_H

#include <gio/gio.h>
#include "http-forwarder.h"
#include "sctp-connection.h"


/*
 * Type macros
 */
#define TYPE_TUNNEL_SERVER_ENDPOINT             (tunnel_server_endpoint_get_type ())
#define TUNNEL_SERVER_ENDPOINT(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_TUNNEL_SERVER_ENDPOINT, TunnelServerEndpoint))
#define IS_TUNNEL_SERVER_ENDPOINT(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_TUNNEL_SERVER_ENDPOINT))
#define TUNNEL_SERVER_ENDPOINT_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_TUNNEL_SERVER_ENDPOINT, TunnelServerEndpointClass))
#define IS_TUNNEL_SERVER_ENDPOINT_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_TUNNEL_SERVER_ENDPOINT))
#define TUNNEL_SERVER_ENDPOINT_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_TUNNEL_SERVER_ENDPOINT, TunnelServerEndpointClass))


typedef struct _TunnelServerEndpoint TunnelServerEndpoint;
typedef struct _TunnelServerEndpointClass TunnelServerEndpointClass;


struct _TunnelServerEndpoint
{
	GObject parent_instance;

	/* instance members */
	SctpConnection *connection;
	GSequence *forwarder_load_queue;
	struct stream_info *stream_list;
	GHashTable *attached_forwarders;
};


struct _TunnelServerEndpointClass
{
	GObjectClass parent_class;
};


GType
tunnel_server_endpoint_get_type(void);

TunnelServerEndpoint*
tunnel_server_endpoint_new(GInetSocketAddress *sock_addr);

#endif
