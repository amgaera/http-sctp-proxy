About
=====

*http-sctp-proxy* is a project providing a SOCKS4 proxy and an SCTP
server that work together to tunnel HTTP requests over SCTP.

The proxy runs on the client system and is the application that
HTTP-enabled programs connect to. It parses all received requests and
tries to forward them to the destination server using SCTP. If it
doesn't succeed, it falls back to TCP.

The SCTP server is deployed on hosts delivering web content. It listens
for incoming HTTP-SCTP requests (as sent by the proxy), decodes and
passes them to the HTTP server running on the same system.

Rationale
=========

HTTP is generally used in conjunction with TCP, which can handle only
a single data stream per connection. As a result, if a client wants to
make several parallel requests to a web server, it must either open as
many TCP connections to the server, or pipeline some requests on one
connection. The former approach is decidedly more resource-intensive,
the latter — complex.

Request pipelining, although available in nearly all web browsers, is
susceptible to head-of-line blocking and unsupported by some web servers
and proxies. As such, it is usually disabled by default.

What is more, browsers typically only open a small number (6 in Firefox
22) of TCP connections to one server, delaying requests over the limit
until a connection is free.

Considering these shortcomings of TCP as a transport for HTTP traffic,
SCTP might seem like the perfect replacement: it can handle multiple
data streams per connection, supports both reliable and unreliable
message delivery, and provides for multihoming.

The aim of this project is to investigate if SCTP does in practice
provide any advantages over TCP for transporting HTTP requests.

Dependencies
============

*http-sctp-proxy* has the following run-time dependencies:

- libsctp
- liblzma
- libsoup (>=2.4)
- libglib (>=2.24)
- libgio (>=2.24)

In order to build it, the development headers of the above libraries, as
well as SCons (>=2.0), must be installed.

Compilation
===========

You can compile and run *http-sctp-proxy* with the following commands::

        scons
        ./socks-proxy
        ./sctp-server
